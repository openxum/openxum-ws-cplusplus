/**
 * @file test/main_yinsh_minimal.cpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2024 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>

#include <openxum/core/games/yinsh/engine.hpp>
#include <openxum/core/games/yinsh/game_type.hpp>
#include <openxum/ai/specific/yinsh/random_player.hpp>
#include <openxum/ai/specific/yinsh/mcts_player.hpp>

using namespace openxum::core::games;

yinsh::Color play() {
  auto engine = new yinsh::Engine(yinsh::REGULAR, yinsh::Color::BLACK);

  std::shared_ptr<openxum::core::common::Player<yinsh::Decision>> player_one = std::make_shared<openxum::ai::specific::yinsh::RandomPlayer>(
    yinsh::Color::BLACK, yinsh::Color::WHITE, engine->clone());
//  std::shared_ptr<openxum::core::common::Player<yinsh::Decision>> player_two = std::make_shared<openxum::ai::specific::yinsh::RandomPlayer>(
//    yinsh::Color::WHITE, yinsh::Color::BLACK, engine->clone());
  std::shared_ptr<openxum::core::common::Player<yinsh::Decision>> player_two = std::make_shared<openxum::ai::specific::yinsh::MCTSPlayer>(
    yinsh::Color::WHITE, yinsh::Color::BLACK, engine->clone());
  auto current_player = player_one;
  auto other_player = player_two;
  unsigned int turn_number = 0;

  std::cout << engine->to_string() << std::endl;

  while (not engine->is_finished()) {
    const openxum::core::common::Move<yinsh::Decision> move = current_player->get_move();
    const int color = engine->current_color();

//    std::cout << turn_number << ": ["
//              << (engine->current_color() == komivoki::Color::BLACK ? "black" : "white")
//              << "] => " << move.to_string() << std::endl;

    std::cout << move.to_string() << std::endl;

    engine->move(move);
    player_one->move(move);
    player_two->move(move);

    std::cout << engine->to_string() << std::endl;

    if (engine->current_color() == player_one->color()) {
      current_player = player_one;
      other_player = player_two;
    } else {
      current_player = player_two;
      other_player = player_one;
    }
    if (color != engine->current_color()) { ++turn_number; }
  }

  auto winner = (yinsh::Color) engine->winner_is();

  std::cout << "  - move number: " << engine->move_number() << std::endl;

  delete engine;
  return winner;
  return yinsh::Color::NONE;
}

int main(int, const char **) {
  const unsigned int game_number = 1;
  unsigned int black_wins = 0;
  unsigned int white_wins = 0;
  unsigned int null_wins = 0;

  for (unsigned int i = 0; i < game_number; ++i) {
    std::cout << (i + 1) << ": " << std::endl;

    yinsh::Color winner = play();

    std::cout << "  - winner: "
              << (winner == yinsh::Color::BLACK ? "black" : (winner == yinsh::Color::WHITE ? "white" : "none"))
              << std::endl;

    if (winner == yinsh::Color::BLACK) {
      ++black_wins;
    } else if (winner == yinsh::Color::WHITE) {
      ++white_wins;
    } else {
      ++null_wins;
    }
  }
  std::cout << "black = " << (((double) black_wins / game_number) * 100) << "%" << std::endl
            << "white = " << (((double) white_wins / game_number) * 100) << "%" << std::endl
            << "null = " << (((double) null_wins / game_number) * 100) << "%" << std::endl;
  return EXIT_SUCCESS;
}