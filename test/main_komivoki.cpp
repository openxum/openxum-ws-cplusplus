/**
 * @file test/main_komivoki.cpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2024 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//#include <iostream>
#include <fstream>
#include "thread_pool.hpp"

#include <openxum/core/games/komivoki/engine.hpp>
#include <openxum/core/games/komivoki/game_type.hpp>
#include <openxum/ai/specific/komivoki/mcts_player.hpp>
#include <openxum/ai/specific/komivoki/random_player.hpp>
#include <openxum/ai/common/random_player.hpp>

using namespace openxum::core::games;

std::ofstream output_file("result.csv");
std::mutex mutex;

void play() {
  auto engine = new komivoki::Engine(komivoki::VARIANT_NO_CONNECT, komivoki::Color::BLACK, {5, 5}, 2,
                                     komivoki::TwoCompleteLineBoardInitializer());
  auto player_one = new openxum::ai::specific::komivoki::MCTSPlayer(komivoki::Color::BLACK, komivoki::Color::WHITE,
                                                                      engine->clone());
  auto player_two = new openxum::ai::specific::komivoki::MCTSPlayer(komivoki::Color::WHITE, komivoki::Color::BLACK,
                                                                    engine->clone());

  openxum::core::common::Player<komivoki::Decision> *current_player = player_one;
  openxum::core::common::Player<komivoki::Decision> *other_player = player_two;
  unsigned int possible_move_number = 0;
  unsigned int turn_number = 0;

  std::map<int, std::vector<unsigned long>> sizes;
  std::map<int, std::vector<double>> gains;
  std::vector<std::string> moves;
  std::map<int, std::vector<double>> distances;
  std::map<int, std::vector<unsigned int>> distance_evaluations;

  sizes[komivoki::Color::BLACK] = std::vector<unsigned long>();
  sizes[komivoki::Color::WHITE] = std::vector<unsigned long>();
  gains[komivoki::Color::BLACK] = std::vector<double>();
  gains[komivoki::Color::WHITE] = std::vector<double>();

  while (not engine->is_finished()) {
    openxum::core::common::Move<komivoki::Decision> move = current_player->get_move();
    int color = engine->current_color();

    // STATS
    possible_move_number += engine->get_possible_move_list().size();
    sizes[engine->current_color()].push_back(engine->get_possible_move_list().size());
    moves.push_back(move.encode());

    engine->move(move);
    current_player->move(move);
    other_player->move(move);

    if (engine->current_color() == player_one->color()) {
      current_player = player_one;
      other_player = player_two;
    } else {
      current_player = player_two;
      other_player = player_one;
    }

    if (color != engine->current_color()) {
      // STATS
      int opponent_color =
        engine->current_color() == komivoki::Color::BLACK ? komivoki::Color::WHITE : komivoki::Color::BLACK;

      gains[engine->current_color()].push_back(engine->gain(engine->current_color(), false));
      gains[opponent_color].push_back(engine->gain(opponent_color, false));
      if (color == komivoki::Color::BLACK) {
        distances[color].push_back(player_one->get_next_goal_distance());
        distance_evaluations[color].push_back(player_one->get_next_goal_distance_evaluation());
      } else {
        distances[color].push_back(player_two->get_next_goal_distance());
        distance_evaluations[color].push_back(player_two->get_next_goal_distance_evaluation());
      }
      ++turn_number;
    }
  }

  {
    std::unique_lock<std::mutex> lock(mutex);

    output_file << "S[BLACK];";
    for (unsigned long e: sizes[komivoki::Color::BLACK]) {
      output_file << e << ";";
    }
    output_file << std::endl;
    output_file << "S[WHITE];";
    for (unsigned long e: sizes[komivoki::Color::WHITE]) {
      output_file << e << ";";
    }
    output_file << std::endl;
    output_file << "G[BLACK];";
    for (double e: gains[komivoki::Color::BLACK]) {
      output_file << e << ";";
    }
    output_file << std::endl;
    output_file << "G[WHITE];";
    for (double e: gains[komivoki::Color::WHITE]) {
      output_file << e << ";";
    }
    output_file << std::endl;
    output_file << "D[BLACK];";
    for (double e: distances[komivoki::Color::BLACK]) {
      output_file << e << ";";
    }
    output_file << std::endl;
    output_file << "D[WHITE];";
    for (double e: distances[komivoki::Color::WHITE]) {
      output_file << e << ";";
    }
    output_file << std::endl;
    output_file << "E[BLACK];";
    for (double e: distance_evaluations[komivoki::Color::BLACK]) {
      output_file << e << ";";
    }
    output_file << std::endl;
    output_file << "E[WHITE];";
    for (double e: distance_evaluations[komivoki::Color::WHITE]) {
      output_file << e << ";";
    }
    output_file << std::endl;
    output_file << "X;";
    for (const std::string &m: moves) {
      output_file << m << ";";
    }
    output_file << std::endl;

    output_file << "T;" << engine->move_number() << ";" << possible_move_number << ";"
                << turn_number << ";"
                << (engine->winner_is() == komivoki::Color::BLACK ? "b"
                                                                  : "w")
                << std::endl;
    output_file.flush();
  }

  delete player_one;
  delete player_two;
}

int main(int, const char **) {
  unsigned int max = std::thread::hardware_concurrency();
  ThreadPool pool(max);
  std::vector<std::future<void> > results;

  for (int i = 0; i < 10; ++i) {
    results.emplace_back(pool.enqueue([=] { play(); }));
  }

  for (auto &&result: results) {
    result.get();
  }

  output_file.close();
  return EXIT_SUCCESS;
}