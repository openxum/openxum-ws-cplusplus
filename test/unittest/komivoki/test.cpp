#define CATCH_CONFIG_MAIN

#include "../catch.hpp"

#include <iostream>

#include <openxum/core/games/komivoki/engine.hpp>

using namespace openxum::core;
using namespace openxum::core::games::komivoki;

TEST_CASE("init board with complete line initializer", "komivoki")
{
  Engine engine(GameType::VARIANT_1, Color::BLACK, {6, 12}, 3,
                RandomBoardInitializer(10, {1, 3}, 772653));

  std::cout << engine.to_string() << std::endl;

  REQUIRE(true);
}

TEST_CASE("no return to previous state - VARIANTE 1", "komivoki")
{
  struct MyBoardInitializer : public BoardInitializer {
    void operator()(Engine &engine) const override {
      engine.put_entity(Coordinates(2, 2), new Entity(Color::BLACK, 1));
      engine.put_entity(Coordinates(2, 2), new Entity(Color::BLACK, 2));

      engine.put_entity(Coordinates(4, 4), new Entity(Color::WHITE, 1));
    }
  };

  Engine engine(GameType::VARIANT_1, Color::BLACK, {8, 8}, 3, MyBoardInitializer());
  common::Move<Decision> first_move(Decision(DecisionType::MOVE, Color::BLACK, Coordinates(2, 2), Coordinates(2, 3)));
  common::Move<Decision> second_move(Decision(DecisionType::MOVE, Color::WHITE, Coordinates(4, 4), Coordinates(5, 5)));

  auto list1 = engine.get_possible_move_list();

  REQUIRE(list1.size() == 8);

  engine.move(first_move);

  auto list2 = engine.get_possible_move_list();

  REQUIRE(list2.size() == 8);

  engine.move(second_move);

  auto list3 = engine.get_possible_move_list();

  REQUIRE(list3.size() == 15);
}

TEST_CASE("mno return to previous state -case 2 - VARIANTE 1", "komivoki")
{
  struct MyBoardInitializer : public BoardInitializer {
    void operator()(Engine &engine) const override {
      engine.put_entity(Coordinates(0, 0), new Entity(Color::BLACK, 1));

      engine.put_entity(Coordinates(3, 6), new Entity(Color::WHITE, 1));
      engine.put_entity(Coordinates(3, 6), new Entity(Color::WHITE, 2));
      engine.put_entity(Coordinates(4, 6), new Entity(Color::WHITE, 1));
      engine.put_entity(Coordinates(4, 6), new Entity(Color::WHITE, 1));
      engine.put_entity(Coordinates(4, 6), new Entity(Color::WHITE, 2));
    }
  };

  Engine engine(GameType::VARIANT_1, Color::BLACK, {8, 8}, 3, MyBoardInitializer());

  common::Move<Decision> first_move(Decision(DecisionType::MOVE, Color::BLACK, Coordinates(0, 0), Coordinates(1, 1)));
  common::Move<Decision> second_move(Decision(DecisionType::MOVE, Color::WHITE, Coordinates(4, 6), Coordinates(3, 6)));
  common::Move<Decision> third_move(Decision(DecisionType::MOVE, Color::BLACK, Coordinates(1, 1), Coordinates(2, 2)));

  engine.move(first_move);
  engine.move(second_move);
  engine.move(third_move);

  REQUIRE(engine.get_possible_move_list().size() == 14);

}

TEST_CASE("no return to previous state - VARIANTE 2", "komivoki")
{
  struct MyBoardInitializer : public BoardInitializer {
    void operator()(Engine &engine) const override {
      engine.put_entity(Coordinates(2, 2), new Entity(Color::BLACK, 1));
      engine.put_entity(Coordinates(2, 2), new Entity(Color::BLACK, 2));

      engine.put_entity(Coordinates(4, 4), new Entity(Color::WHITE, 1));
    }
  };

  Engine engine(GameType::VARIANT_2, Color::BLACK, {8, 8}, 3, MyBoardInitializer());
  common::Move<Decision> first_move(Decision(DecisionType::MOVE, Color::BLACK, Coordinates(2, 2), Coordinates(2, 3)));
  common::Move<Decision> second_move(Decision(DecisionType::MOVE, Color::WHITE, Coordinates(4, 4), Coordinates(5, 5)));

  auto list1 = engine.get_possible_move_list();

  REQUIRE(list1.size() == 8);

  engine.move(first_move);

  auto list2 = engine.get_possible_move_list();

  REQUIRE(list2.size() == 8);

  engine.move(second_move);

//  std::cout << engine.to_string() << std::endl;

  auto list3 = engine.get_possible_move_list();

//  std::for_each(list3.cbegin(), list3.cend(), [](const auto& e) {
//    std::cout << e.to_string() << std::endl;
//  });

  REQUIRE(list3.size() == 9);

}

TEST_CASE("move rule - VARIANTE 2", "komivoki")
{
  struct MyBoardInitializer : public BoardInitializer {
    void operator()(Engine &engine) const override {
      engine.put_entity(Coordinates(4, 0), new Entity(Color::BLACK, 1));
      engine.put_entity(Coordinates(3, 1), new Entity(Color::BLACK, 1));
      engine.put_entity(Coordinates(4, 1), new Entity(Color::BLACK, 1));

      engine.put_entity(Coordinates(5, 5), new Entity(Color::WHITE, 1));
    }
  };

  Engine engine(GameType::VARIANT_2, Color::BLACK, {8, 8}, 3, MyBoardInitializer());

  REQUIRE(engine.get_possible_move_list().size() == 17);
}