/**
 * @file test/main_komivoki_minimal.cpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2024 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>

#include <openxum/core/games/komivoki/engine.hpp>
#include <openxum/core/games/komivoki/game_type.hpp>
#include <openxum/ai/specific/komivoki/expert_player.hpp>
#include <openxum/ai/specific/komivoki/mcts_player.hpp>
#include <openxum/ai/specific/komivoki/random_player.hpp>

using namespace openxum::core::games;

komivoki::Color play() {
//  komivoki::FixedBoardInitializer::Configuration configuration = {
//    {{komivoki::BLACK, 1}, {-1, 0}, {-1, 0}, {-1, 0}, {-1, 0}},
//    {{-1, 0}, {-1, 0}, {-1, 0}, {-1, 0}, {-1, 0}},
//    {{-1, 0}, {-1, 0}, {-1, 0}, {-1, 0}, {-1, 0}},
//    {{-1, 0}, {-1, 0}, {-1, 0}, {-1, 0}, {-1, 0}},
//    {{-1, 0}, {-1, 0}, {-1, 0}, {-1, 0}, {komivoki::WHITE, 1}}
//  };
//  auto engine = new komivoki::Engine(komivoki::VARIANT_NO_CONNECT| komivoki::VARIANT_CAPTURE_REMOVE | komivoki::VARIANT_MOVE_TOP_ENTITY,
//                                     komivoki::Color::BLACK, {5, 5}, 2,
//                                     komivoki::FixedBoardInitializer(configuration),
//                                     komivoki::eight_deltas);
  auto engine = new komivoki::Engine(komivoki::VARIANT_NO_CONNECT| komivoki::VARIANT_CAPTURE_REMOVE | komivoki::VARIANT_MOVE_TOP_ENTITY,
                                     komivoki::Color::BLACK, {6, 6}, 2,
                                     komivoki::CornerBoardInitializer(3),
                                     komivoki::eight_deltas);
//  auto engine = new komivoki::Engine(komivoki::VARIANT_NO_CONNECT| komivoki::VARIANT_CAPTURE_REMOVE | komivoki::VARIANT_MOVE_STACK,
//                                     komivoki::Color::BLACK, {8, 8}, 3,
//                                     komivoki::RandomBoardInitializer(10, {1, 3}, 772653),
//                                     komivoki::eight_deltas);
//  auto engine = new komivoki::Engine(komivoki::VARIANT_NO_CONNECT | komivoki::VARIANT_CAPTURE_REMOVE | komivoki::VARIANT_MOVE_STACK,
//                                     komivoki::Color::BLACK, {8, 8}, 3,
//                                     komivoki::TwoCompleteLineBoardInitializer(), komivoki::eight_deltas);
//  std::shared_ptr<openxum::core::common::Player<komivoki::Decision>> player_one = std::make_shared<openxum::ai::specific::komivoki::RandomPlayer>(
//    komivoki::Color::BLACK,
//    komivoki::Color::WHITE,
//    engine->clone());
//  std::shared_ptr<openxum::core::common::Player<komivoki::Decision>> player_two = std::make_shared<openxum::ai::specific::komivoki::RandomPlayer>(
//    komivoki::Color::WHITE,
//    komivoki::Color::BLACK,
//    engine->clone());
  std::shared_ptr<openxum::core::common::Player<komivoki::Decision>> player_one = std::make_shared<openxum::ai::specific::komivoki::MCTSPlayer>(
    komivoki::Color::BLACK,
    komivoki::Color::WHITE,
    engine->clone());
//  std::shared_ptr<openxum::core::common::Player<komivoki::Decision>> player_two = std::make_shared<openxum::ai::specific::komivoki::ExpertPlayer>(
//    komivoki::Color::WHITE,
//    komivoki::Color::BLACK,
//    engine->clone());
  std::shared_ptr<openxum::core::common::Player<komivoki::Decision>> player_two = std::make_shared<openxum::ai::specific::komivoki::MCTSPlayer>(
    komivoki::Color::WHITE,
    komivoki::Color::BLACK,
    engine->clone());
  auto current_player = player_one;
  auto other_player = player_two;
  unsigned int turn_number = 0;

//  std::cout << "init:" << std::endl;
//  std::cout << engine->to_string() << std::endl;

  while (not engine->is_finished()) {
    const openxum::core::common::Move<komivoki::Decision> move = current_player->get_move();
    const int color = engine->current_color();

//    std::cout << turn_number << ": ["
//              << (engine->current_color() == komivoki::Color::BLACK ? "black" : "white")
//              << "] => " << move.to_string() << std::endl;

    engine->move(move);
    player_one->move(move);
    player_two->move(move);

//    std::cout << move.to_string() << std::endl;
//    std::cout << engine->to_string() << std::endl;

    if (engine->current_color() == player_one->color()) {
      current_player = player_one;
      other_player = player_two;
    } else {
      current_player = player_two;
      other_player = player_one;
    }
    if (color != engine->current_color()) { ++turn_number; }
  }

  auto winner = (komivoki::Color) engine->winner_is();

  std::cout << "  - move number: " << engine->move_number() << std::endl;

  delete engine;
  return winner;
}

int main(int, const char **) {
  const unsigned int game_number = 100;
  unsigned int black_wins = 0;
  unsigned int white_wins = 0;
  unsigned int null_wins = 0;

  for (unsigned int i = 0; i < game_number; ++i) {
    std::cout << (i + 1) << ": " << std::endl;

    komivoki::Color winner = play();

    std::cout << "  - winner: "
              << (winner == komivoki::Color::BLACK ? "black" : (winner == komivoki::Color::WHITE ? "white" : "none"))
              << std::endl;

    if (winner == komivoki::Color::BLACK) {
      ++black_wins;
    } else if (winner == komivoki::Color::WHITE) {
      ++white_wins;
    } else {
      ++null_wins;
    }
  }
  std::cout << "black = " << (((double) black_wins / game_number) * 100) << "%" << std::endl
            << "white = " << (((double) white_wins / game_number) * 100) << "%" << std::endl
            << "null = " << (((double) null_wins / game_number) * 100) << "%" << std::endl;
  return EXIT_SUCCESS;
}