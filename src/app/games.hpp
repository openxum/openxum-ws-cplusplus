/**
 * @file openxum/app/games.hpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2024 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPENXUM_WS_CPP_GAMES_HPP
#define OPENXUM_WS_CPP_GAMES_HPP

#include <openxum/ai/specific/kamisado/random_player.hpp>
#include <openxum/ai/specific/kamisado/mcts_player.hpp>

#include <openxum/ai/specific/kikotsoka/mcts_player.hpp>
#include <openxum/ai/specific/kikotsoka/random_player.hpp>

#include <openxum/ai/specific/kikotsoka-polyomino/mcts_player.hpp>
#include <openxum/ai/specific/kikotsoka-polyomino/random_player.hpp>

#include <openxum/ai/specific/komivoki/mcts_player.hpp>
#include <openxum/ai/specific/komivoki/random_player.hpp>

#endif //OPENXUM_WS_CPP_GAMES_HPP
