/**
 * @file openxum/ai/specific/komivoki/mcts_player.hpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2024 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPENXUM_AI_SPECIFIC_KOMIVOKI_MCTS_PLAYER_HPP
#define OPENXUM_AI_SPECIFIC_KOMIVOKI_MCTS_PLAYER_HPP

#include <openxum/ai/common/mcts_player.hpp>
#include <openxum/core/games/komivoki/decision.hpp>
#include <openxum/core/games/komivoki//engine.hpp>
#include <openxum/core/common/builder.hpp>

namespace openxum::ai::specific::komivoki {

class MCTSPlayer : public openxum::ai::common::MCTSPlayer<core::games::komivoki::Decision> {
public:
  MCTSPlayer(int c, int o, openxum::core::common::TwoPlayerEngine<core::games::komivoki::Decision> *e)
    : openxum::ai::common::MCTSPlayer<core::games::komivoki::Decision>(c, o, e, 1000) {}
};

}

DECLARE_GAME(komivoki, mcts, MCTSPlayer)

#endif