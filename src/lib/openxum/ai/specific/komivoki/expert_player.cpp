#include "expert_player.hpp"

namespace openxum::ai::specific::komivoki {

openxum::core::common::Move<openxum::core::games::komivoki::Decision> ExpertPlayer::get_move() {
  const auto &e = dynamic_cast<const openxum::core::games::komivoki::Engine &>(this->engine());
  const auto &list = e.get_possible_move_list();
  openxum::core::common::Move<openxum::core::games::komivoki::Decision> move(
    (openxum::core::games::komivoki::Decision(openxum::core::games::komivoki::DecisionType::PASS,
                                              openxum::core::games::komivoki::Color(e.current_color()))));

  if (not list.empty()) {
    double min = 100000;

    for (const auto &m: list) {
      double score = evaluate(m);

      if (min > score) {
        move = m;
        min = score;
      }
    }
  }
  return move;
}

double ExpertPlayer::evaluate(const openxum::core::common::Move<openxum::core::games::komivoki::Decision> &move) const {
  auto e_clone = dynamic_cast<openxum::core::games::komivoki::Engine *>(this->engine().clone());
  const auto &board = e_clone->board();
  std::vector<double> scores;
  std::vector<double> opponent_scores;

  e_clone->move(move);
  for (unsigned int y = 0; y < e_clone->size().second; ++y) {
    for (unsigned int x = 0; x < e_clone->size().first; ++x) {
      const auto &cell = board.at(x + y * e_clone->size().first);

      if (not cell.empty()) {
        unsigned int capacity = 0;

        std::for_each(cell.cbegin(), cell.cend(), [&capacity](const auto &e) { capacity += e->level; });
        if (cell.back()->color != this->color()) {
          if (x == 0 or x == e_clone->size().first - 1 or y == 0 or y == e_clone->size().second - 1) {
            opponent_scores.push_back(capacity / 2.);
          } else {
            opponent_scores.push_back(capacity);
          }
          scores.push_back(100.);
        } else {
          opponent_scores.push_back(100.);
          if (x == 0 or x == e_clone->size().first - 1 or y == 0 or y == e_clone->size().second - 1) {
            scores.push_back(capacity / 2.);
          } else {
            scores.push_back(capacity);
          }
        }
      } else {
        opponent_scores.push_back(100.);
        scores.push_back(100.);
      }
    }
  }
  for (unsigned int x = 0; x < e_clone->size().first; ++x) {
    for (unsigned int y = 0; y < e_clone->size().second; ++y) {
      const auto &cell = board.at(x + y * e_clone->size().first);

      if (not cell.empty() and cell.back()->color == this->color()) {
        for (unsigned int dx = 0; dx < e_clone->size().first; ++dx) {
          for (unsigned int dy = 0; dy < e_clone->size().second; ++dy) {
            const auto &opponent_cell = board.at(dx + dy * e_clone->size().first);
            unsigned int capacity = 0;

            std::for_each(cell.cbegin(), cell.cend(), [&capacity](const auto &e) { capacity += e->level; });
            if (not opponent_cell.empty() and opponent_cell.back()->color != this->color()) {
              double distance = std::sqrt((dx - x) * (dx - x) + (dy - y) * (dy - y));
              unsigned int opponent_capacity = 0;

              std::for_each(cell.cbegin(), cell.cend(),
                            [&opponent_capacity](const auto &e) { opponent_capacity += e->level; });
              if (distance < 2) {
                opponent_scores[dx + dy * e_clone->size().first] -= capacity * 1.8;
                scores[x + y * e_clone->size().first] -= opponent_capacity * 1.5;
              } else {
                opponent_scores[dx + dy * e_clone->size().first] -= capacity / distance;
                scores[x + y * e_clone->size().first] -= opponent_capacity / distance;
              }
            }
          }
        }
      }
    }
  }
  double opponent_score = *std::min_element(opponent_scores.cbegin(), opponent_scores.cend());
  double score = *std::min_element(scores.cbegin(), scores.cend());

  delete e_clone;
  return opponent_score + (1 - score);
}

DEFINE_GAME(komivoki, expert)

}