/**
 * @file openxum/ai/specific/kikotsoka-polyomino/mcts_player.hpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2024 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPENXUM_AI_SPECIFIC_KIKOTSOKA_POLYOMINO_MCTS_PLAYER_HPP
#define OPENXUM_AI_SPECIFIC_KIKOTSOKA_POLYOMINO_MCTS_PLAYER_HPP

#include <openxum/ai/common/mcts_player.hpp>
#include <openxum/core/games/kikotsoka-polyomino/decision.hpp>
#include <openxum/core/games/kikotsoka-polyomino//engine.hpp>
#include <openxum/core/common/builder.hpp>

namespace openxum::ai::specific::kikotsoka_polyomino {

class MCTSPlayer : public openxum::ai::common::MCTSPlayer<core::games::kikotsoka_polyomino::Decision> {
public:
  MCTSPlayer(int c,
             int o,
             openxum::core::common::TwoPlayerEngine<core::games::kikotsoka_polyomino::Decision> *e,
             unsigned int simulation_number = 1000,
             bool stoppable = false)
    : openxum::ai::common::MCTSPlayer<core::games::kikotsoka_polyomino::Decision>(c, o, e, simulation_number,
                                                                                  stoppable) {}
};

}

DECLARE_GAME(kikotsoka_polyomino, mcts, MCTSPlayer)

#endif