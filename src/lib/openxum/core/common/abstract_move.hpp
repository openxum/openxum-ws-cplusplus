/**
 * @file openxum/core/common/abstract_move.hpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2024 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPENXUM_CORE_COMMON_ABSTRACT_MOVE_HPP
#define OPENXUM_CORE_COMMON_ABSTRACT_MOVE_HPP

#include <nlohmann/json.hpp>
#include <vector>

namespace openxum::core::common {

class AbstractMove {
public:
  AbstractMove() = default;

  virtual ~AbstractMove() = default;

  virtual AbstractMove *clone() const = 0;

  virtual void decode(const std::string &str) = 0;

  virtual std::string encode() const = 0;

  virtual void from_object(const nlohmann::json &json) = 0;

  virtual nlohmann::json to_object() const = 0;

  virtual std::string to_string() const = 0;
};

}

#endif // OPENXUM_CORE_COMMON_ABSTRACT_MOVE_HPP