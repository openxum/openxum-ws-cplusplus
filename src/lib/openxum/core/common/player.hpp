/**
 * @file openxum/core/common/player.hpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2024 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPENXUM_CORE_COMMON_PLAYER_HPP
#define OPENXUM_CORE_COMMON_PLAYER_HPP

#include <nlohmann/json.hpp>

#include <openxum/core/common/abstract_player.hpp>
#include <openxum/core/common/move.hpp>
#include <openxum/core/common/two_player_engine.hpp>

namespace openxum::core::common {

template<class Decision>
class Player : public AbstractPlayer {
public:
  Player(int color, int opponent_color, TwoPlayerEngine<Decision> *engine)
    : AbstractPlayer(color, opponent_color, engine) {}

  ~Player() override = default;

  const TwoPlayerEngine<Decision> &engine() const { return *dynamic_cast<TwoPlayerEngine<Decision> *>(_engine.get()); }

  TwoPlayerEngine<Decision> &engine() { return *dynamic_cast<TwoPlayerEngine<Decision> *>(_engine.get()); }

  virtual Move<Decision> get_move() = 0;

  virtual double get_next_goal_distance() = 0;

  virtual unsigned int get_next_goal_distance_evaluation() = 0;

  nlohmann::json move() override {
    return get_move().to_object();
  }

  void move(const Move<Decision> &move) { engine().move(move); }
};

#define DECLARE_GAME(game_name, player_type, player) class Builder_##player_type##_##game_name : public openxum::core::common::GameBuilder { \
public:                                                                                                                                      \
  Builder_##player_type##_##game_name() { }                                                                                                  \
  openxum::core::common::AbstractPlayer* operator()(int player_color, int opponent_color, int type, int color) override {                    \
    return new openxum::ai::specific::game_name::player(                                                                                     \
      player_color, opponent_color, new openxum::core::games::game_name::Engine(type, color));                                               \
  }                                                                                                                                          \
  ~Builder_##player_type##_##game_name() = default;                                                                                          \
};                                                                                                                                           \
                                                                                                                                             \
class Helper_##player_type##_##game_name {                                                                                                   \
public:                                                                                                                                      \
  Helper_##player_type##_##game_name() { openxum::core::common::Builder::instance().insert(#game_name, #player_type, new Builder_##player_type##_##game_name); } \
};

#define DEFINE_GAME(game_name, player_type) Helper_##player_type##_##game_name helper_##player_type##_##game_name;

}

#endif