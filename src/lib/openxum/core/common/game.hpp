/**
 * @file openxum/core/common/game.hpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2024 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPENXUM_CORE_COMMON_GAME_HPP
#define OPENXUM_CORE_COMMON_GAME_HPP

#include <openxum/core/common/two_player_engine.hpp>
#include <openxum/core/common/player.hpp>

namespace openxum::core::common {

struct Stats {
  unsigned int possible_move_number = 0;
  unsigned int turn_number = 0;

  std::map<int, std::vector<unsigned long>> sizes;
  std::map<int, std::vector<double>> gains;
  std::vector<std::string> moves;
  std::map<int, std::vector<double>> distances;
  std::map<int, std::vector<unsigned int>> distance_evaluations;
};

template<typename Decision>
class Game {
public:
  Game(openxum::core::common::TwoPlayerEngine<Decision> *engine,
       const std::shared_ptr<openxum::core::common::Player<Decision>> &p1,
       const std::shared_ptr<openxum::core::common::Player<Decision>> &p2) : _engine(engine), _first_player(p1),
                                                                             _second_player(p2) {}

  int run(bool display = false) {
    _current_player = _first_player;
    _other_player = _second_player;
    while (not _engine->is_finished()) {
      step(display);
    }
    return _engine->winner_is();
  }

  std::pair<int, Stats> run_with_stats(bool display = false) {
    Stats stats;
    int first_player_color = _first_player->color();
    int second_player_color = _second_player->color();

    _current_player = _first_player;
    _other_player = _second_player;
    while (not _engine->is_finished()) {
      const auto list = _engine->get_possible_move_list();
      const int color = _engine->current_color();

      stats.sizes[_engine->current_color()].push_back(list.size());
      stats.possible_move_number += list.size();

      const Move<Decision> move = step(display);

      stats.moves.push_back(move.encode());
      if (color != _engine->current_color()) {
        stats.gains[first_player_color].push_back(_engine->gain(first_player_color, false));
        stats.gains[second_player_color].push_back(_engine->gain(second_player_color, false));
        if (color == first_player_color) {
          stats.distances[color].push_back(_first_player->get_next_goal_distance());
          stats.distance_evaluations[color].push_back(_first_player->get_next_goal_distance_evaluation());
        } else {
          stats.distances[color].push_back(_second_player->get_next_goal_distance());
          stats.distance_evaluations[color].push_back(_second_player->get_next_goal_distance_evaluation());
        }
        ++stats.turn_number;
      }
    }
    return {_engine->winner_is(), stats};
  }

private:
  openxum::core::common::Move<Decision> step(bool display) {
    const openxum::core::common::Move<Decision> move = _current_player->get_move();

    if (display) {
      std::cout << move.to_string() << std::endl;
    }

    _engine->move(move);

    if (display) {
      std::cout << _engine->to_string() << std::endl;
    }

    _current_player->move(move);
    _other_player->move(move);
    if (_engine->current_color() == _first_player->color()) {
      _current_player = _first_player;
      _other_player = _second_player;
    } else {
      _current_player = _second_player;
      _other_player = _first_player;
    }
    return move;
  }

  openxum::core::common::TwoPlayerEngine<Decision> *_engine;
  std::shared_ptr<openxum::core::common::Player<Decision>> _first_player;
  std::shared_ptr<openxum::core::common::Player<Decision>> _second_player;
  std::shared_ptr<openxum::core::common::Player<Decision>> _current_player;
  std::shared_ptr<openxum::core::common::Player<Decision>> _other_player;
};

}

#endif