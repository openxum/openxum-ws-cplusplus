/**
 * @file openxum/core/common/builder.hpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2024 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPENXUM_WS_CPP_BUILDER_HPP
#define OPENXUM_WS_CPP_BUILDER_HPP

#include <openxum/core/common/game_builder.hpp>

#include <map>

namespace openxum::core::common {

class Builder {
public:
  Builder(const Builder &) = delete;

  AbstractPlayer *build(const std::string &game, const std::string &player_type, int player_color, int opponent_color, int type, int color) {
    return _builders.find(game)->second.find(player_type)->second->operator()(player_color, opponent_color, type, color);
  }

  void insert(const std::string &game, const std::string &player_type, GameBuilder *builder) {
    auto it = _builders.find(game);

    if (it == _builders.end()) {
      _builders.insert(std::make_pair(game, std::map<std::string, std::unique_ptr<GameBuilder>>()));
    }
    _builders[game].insert(std::make_pair(player_type, std::unique_ptr<GameBuilder>(builder)));
  }

  void operator=(const Builder &) = delete;

  static Builder &instance() {
    static Builder instance;

    return instance;
  }

private:
  Builder() = default;

  std::map<std::string, std::map<std::string, std::unique_ptr<GameBuilder>>> _builders;
};

}

#endif //OPENXUM_WS_CPP_BUILDER_HPP
