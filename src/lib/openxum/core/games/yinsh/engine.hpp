/**
 * @file openxum/core/games/yinsh/engine.hpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2024 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPENXUM_CORE_GAMES_YINSH_ENGINE_HPP
#define OPENXUM_CORE_GAMES_YINSH_ENGINE_HPP

#include <openxum/core/common/two_player_engine.hpp>

#include <openxum/core/games/yinsh/color.hpp>
#include <openxum/core/games/yinsh/coordinates.hpp>
#include <openxum/core/games/yinsh/decision.hpp>
#include <openxum/core/games/yinsh/intersection.hpp>
#include <openxum/core/games/yinsh/game_type.hpp>
#include <openxum/core/games/yinsh/phase.hpp>
#include <openxum/core/games/yinsh/row.hpp>

#include <vector>

namespace openxum::core::games::yinsh {

class Engine : public openxum::core::common::TwoPlayerEngine<Decision> {
public:
  Engine() : _type(GameType::REGULAR) {}

  Engine(int type, int color);

  ~Engine() override = default;

  int best_is() const override { return NONE; }

  Engine *clone() const override;

  int current_color() const override;

  double gain(int color, bool /* finish */) const override { return color == Color::BLACK ? _removed_black_ring_number : _removed_white_ring_number; }

  const std::string &get_name() const override { return GAME_NAME; }

  openxum::core::common::Moves<Decision> get_possible_move_list() const override;

  std::string hash() const override;

  bool is_finished() const override;

  bool is_stoppable() const override { return false; }

  void move(const openxum::core::common::Move<Decision> &move) override;

  void parse(const std::string &) override {}

  int type() const { return _type; }

  std::string to_string() const override;

  int winner_is() const override;

private:
  void build_row(char letter, int number, State state, bool &start, Rows &rows, Row &row) const;

  void change_color();

  void flip(char letter, int number);

  void flip(const Intersection &origin, const Intersection &destination);

  std::vector<Coordinates> get_free_intersections() const;

  std::vector<Coordinates> get_possible_moving_list(const Coordinates &origin, Color color, bool control) const;

  Rows get_rows(const Color &color) const;

  void move_ring(const Coordinates &origin, const Coordinates &destination);

  void put_marker(const Coordinates &coordinates, const Color &color);

  void put_ring(const Coordinates &coordinates, const Color &color);

  void remove_black_ring(const Coordinates &coordinates);

  void remove_marker(char letter, int number);

  void remove_ring(const Coordinates &coordinates, const Color &color);

  void remove_row(const Row &row, const Color &color);

  void remove_white_ring(const Coordinates &coordinates);

  void verify_intersection(char letter, int number, bool &ok, bool &no_vacant) const;

  static const std::vector<char> begin_letter;
  static const std::vector<char> end_letter;
  static const std::vector<int> begin_number;
  static const std::vector<int> end_number;
  static const std::vector<char> begin_diagonal_letter;
  static const std::vector<char> end_diagonal_letter;
  static const std::vector<int> begin_diagonal_number;
  static const std::vector<int> end_diagonal_number;
  static const std::vector<char> letters;
  static const char letter_matrix[];
  static const int number_matrix[];

  const int _type;

  Color _current_color;
  unsigned int _marker_number;
  std::vector<Coordinates> _placed_black_ring_coordinates;
  std::vector<Coordinates> _placed_white_ring_coordinates;
  unsigned int _removed_black_ring_number;
  unsigned int _removed_white_ring_number;
  Coordinates _last_marker;
  std::map<int, Intersection> _intersections;
  Phase::values _phase;

  static const std::string GAME_NAME;
};

}

#endif
