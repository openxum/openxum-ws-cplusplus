/**
 * @file openxum/core/games/yinsh/decision.cpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2024 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <openxum/core/games/yinsh/decision.hpp>

namespace openxum::core::games::yinsh {

Decision::Decision(const DecisionType::values &type, const Color &color)
  : _type(type), _color(color) {}

Decision::Decision(const DecisionType::values &type, const Color &color, const Coordinates &to)
  : _type(type), _color(color), _to(to) {}

Decision::Decision(const DecisionType::values &type, const Color &color, const Row &row)
  : _type(type), _color(color), _row(row) {}

Decision::Decision(const DecisionType::values &type, const Color &color, const Coordinates &from, const Coordinates &to)
  : _type(type), _color(color), _from(from), _to(to) {}

void Decision::decode(const std::string &str) {
  std::string type = str.substr(0, 2);

  if (type == "Pr") {
    _type = DecisionType::values::PUT_RING;
  } else if (type == "Pm") {
    _type = DecisionType::PUT_MARKER;
  } else if (type == "Rr") {
    _type = DecisionType::REMOVE_RING;
  } else if (type == "Mr") {
    _type = DecisionType::MOVE_RING;
  } else if (type == "RR") {
    _type = DecisionType::REMOVE_ROW;
  }
  _color = str[2] == 'B' ? Color::BLACK : Color::WHITE;
  if (_type == DecisionType::PUT_RING or _type == DecisionType::PUT_MARKER or _type == DecisionType::REMOVE_RING) {
    _to = Coordinates(str[3], str[4] - '0');
  } else if (_type == DecisionType::MOVE_RING) {
    _from = Coordinates(str[3], str[4] - '0');
    _to = Coordinates(str[5], str[6] - '0');
  } else {
    _row.clear();
    for (unsigned int index = 0; index < 5; ++index) {
      _row.push_back(Coordinates(str[3 + 2 * index], str[4 + 2 * index] - '0'));
    }
  }
}

std::string Decision::encode() const {
  if (_type == DecisionType::PUT_MARKER) {
    return "Pm" + std::string(_color == BLACK ? "B" : "W") + _to.to_string();
  } else if (_type == DecisionType::PUT_RING) {
    return "Pr" + std::string(_color == BLACK ? "B" : "W") + _to.to_string();
  } else if (_type == DecisionType::MOVE_RING) {
    return "Mr" + std::string(_color == BLACK ? "B" : "W") + _from.to_string() + _to.to_string();
  } else if (_type == DecisionType::REMOVE_RING) {
    return "Rr" + std::string(_color == BLACK ? "B" : "W") + _to.to_string();
  } else { // _type == DecisionType::REMOVE_ROW
    std::string str = "RR" + std::string(_color == Color::BLACK ? "B" : "W");

    for (const auto &e: _row) {
      str += e.to_string();
    }
    return str;
  }
}

void Decision::from_object(const nlohmann::json &json) {
  _type = DecisionType::values(json["type"].get<int>());
  _color = Color(json["color"].get<int>());
  if (_type == DecisionType::PUT_RING or _type == DecisionType::PUT_MARKER or _type == DecisionType::REMOVE_RING) {
    Coordinates to;

    to.from_object(json["to"]);
    _to = to;
  } else if (_type == DecisionType::MOVE_RING) {
    Coordinates from, to;

    from.from_object(json["from"]);
    to.from_object(json["to"]);
    _from = from;
    _to = to;
  } else {
    for (const auto& e: json["row"]) {
      Coordinates coordinates;

      coordinates.from_object(e);
      _row.push_back(coordinates);
    }
  }
}

nlohmann::json Decision::to_object() const {
  nlohmann::json json;

  json["type"] = _type;
  json["color"] = _color;
  if (_type == DecisionType::PUT_RING or _type == DecisionType::PUT_MARKER or _type == DecisionType::REMOVE_RING) {
    json["to"] = _to.to_object();
  } else if (_type == DecisionType::MOVE_RING) {
    json["to"] = _to.to_object();
    json["from"] = _from.to_object();
  } else {
    json["row"] = nlohmann::json::array();
    for (const auto &e: _row) {
      json["row"].push_back(e.to_object());
    }
  }
  return json;
}

std::string Decision::to_string() const {
  if (_type == DecisionType::PUT_RING) {
    return std::string("put ") + (_color == Color::BLACK ? "black" : "white") + " ring at " + _to.to_string();
  } else if (_type == DecisionType::PUT_MARKER) {
    return std::string("put ") + (_color == Color::BLACK ? "black" : "white") + " marker at " + _to.to_string();
  } else if (_type == DecisionType::REMOVE_RING) {
    return std::string("remove ") + (_color == Color::BLACK ? "black" : "white") + " ring at " + _to.to_string();
  } else if (_type == DecisionType::MOVE_RING) {
    return std::string("move ") + (_color == Color::BLACK ? "black" : "white") + " ring from " + _from.to_string() +
           " to " + _to.to_string();
  } else {
    std::string str = std::string("remove ") + (_color == Color::BLACK ? "black" : "white") + " row ( ";

    for (const auto &e: _row) {
      str += e.to_string() + " ";
    }
    str += ")";
    return str;
  }
}

}