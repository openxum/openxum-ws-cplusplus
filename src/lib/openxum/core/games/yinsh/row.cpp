/**
 * @file openxum/core/games/yinsh/row.cpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2024 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "row.hpp"

#include <algorithm>
#include <sstream>

namespace openxum::core::games::yinsh {

Row::Row(const Coordinates &begin, const Coordinates &end) {
  build(begin, end);
}

bool Row::belong(const Coordinates &coordinates) const {
  return std::find(begin(), end(), coordinates) != end();
}

bool Row::is_separated(const Row &row) const {
  bool found = false;
  Row::const_iterator it = row.begin();

  while (not found and it != row.end()) {
    found = std::find(begin(), end(), *it) == end();
    ++it;
  }
  return not found;
}

std::string Row::to_string() const {
  std::stringstream str;
  Row::const_iterator it = begin();

  str << "{ ";
  while (it != end()) {
    str << it->letter() << it->number() << " ";
    ++it;
  }
  str << "}";
  return str.str();
}

void Row::build(const Coordinates &begin, const Coordinates &end) {
  if (begin.letter() == end.letter()) {
    if (begin.number() < end.number()) {
      int n = begin.number();

      while (n <= end.number()) {
        push_back(Coordinates(begin.letter(), n));
        ++n;
      }
    } else {
      int n = begin.number();

      while (n >= end.number()) {
        push_back(Coordinates(begin.letter(), n));
        --n;
      }
    }
  } else if (begin.number() == end.number()) {
    if (begin.letter() < end.letter()) {
      char l = begin.letter();

      while (l <= end.letter()) {
        push_back(Coordinates(l, begin.number()));
        ++l;
      }
    } else {
      char l = begin.letter();

      while (l >= end.letter()) {
        push_back(Coordinates(l, begin.number()));
        --l;
      }
    }
  } else if (begin.letter() < end.letter()) {
    int n = begin.number();
    char l = begin.letter();

    while (l <= end.letter()) {
      push_back(Coordinates(l, n));
      ++l;
      ++n;
    }
  } else {
    int n = begin.number();
    char l = begin.letter();

    while (l >= end.letter()) {
      push_back(Coordinates(l, n));
      --l;
      --n;
    }
  }
}

}
