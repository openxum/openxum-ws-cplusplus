/**
 * @file openxum/core/games/yinsh/engine.cpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2024 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <openxum/core/games/yinsh/engine.hpp>

namespace openxum::core::games::yinsh {

const std::vector<char> Engine::begin_letter = {'B', 'A', 'A', 'A', 'A', 'B', 'B', 'C', 'D', 'E', 'G'};
const std::vector<char> Engine::end_letter = {'E', 'G', 'H', 'I', 'J', 'J', 'K', 'K', 'K', 'K', 'J'};
const std::vector<int> Engine::begin_number = {2, 1, 1, 1, 1, 2, 2, 3, 4, 5, 7};
const std::vector<int> Engine::end_number = {5, 7, 8, 9, 10, 10, 11, 11, 11, 11, 10};
const std::vector<char> Engine::begin_diagonal_letter = {'B', 'A', 'A', 'A', 'A', 'B', 'B', 'C', 'D', 'E', 'G'};
const std::vector<char> Engine::end_diagonal_letter = {'E', 'G', 'H', 'I', 'J', 'J', 'K', 'K', 'K', 'K', 'J'};
const std::vector<int> Engine::begin_diagonal_number = {7, 5, 4, 3, 2, 2, 1, 1, 1, 1, 2};
const std::vector<int> Engine::end_diagonal_number = {10, 11, 11, 11, 11, 10, 10, 9, 8, 7, 5};
const std::vector<char> Engine::letters = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K'};
const char Engine::letter_matrix[] = {
  'X', 'X', 'X', 'Z', 'X', 'E', 'X', 'G', 'X', 'X', 'X', 'X',
  'X', 'X', 'Z', 'X', 'D', 'X', 'F', 'X', 'H', 'X', 'X', 'X',
  'X', 'Z', 'X', 'C', 'X', 'E', 'X', 'G', 'X', 'I', 'X', 'X',
  'X', 'X', 'B', 'X', 'D', 'X', 'F', 'X', 'H', 'X', 'J', 'X',
  'X', 'Z', 'X', 'C', 'X', 'E', 'X', 'G', 'X', 'I', 'X', 'X',
  'Z', 'X', 'B', 'X', 'D', 'X', 'F', 'X', 'H', 'X', 'J', 'X',
  'X', 'A', 'X', 'C', 'X', 'E', 'X', 'G', 'X', 'I', 'X', 'K',
  'Z', 'X', 'B', 'X', 'D', 'X', 'F', 'X', 'H', 'X', 'J', 'X',
  'X', 'A', 'X', 'C', 'X', 'E', 'X', 'G', 'X', 'I', 'X', 'K',
  'Z', 'X', 'B', 'X', 'D', 'X', 'F', 'X', 'H', 'X', 'J', 'X',
  'X', 'A', 'X', 'C', 'X', 'E', 'X', 'G', 'X', 'I', 'X', 'K',
  'Z', 'X', 'B', 'X', 'D', 'X', 'F', 'X', 'H', 'X', 'J', 'X',
  'X', 'A', 'X', 'C', 'X', 'E', 'X', 'G', 'X', 'I', 'X', 'K',
  'X', 'X', 'B', 'X', 'D', 'X', 'F', 'X', 'H', 'X', 'J', 'X',
  'X', 'Z', 'X', 'C', 'X', 'E', 'X', 'G', 'X', 'I', 'X', 'X',
  'X', 'X', 'B', 'X', 'D', 'X', 'F', 'X', 'H', 'X', 'J', 'X',
  'X', 'X', 'X', 'C', 'X', 'E', 'X', 'G', 'X', 'I', 'X', 'X',
  'X', 'X', 'X', 'X', 'D', 'X', 'F', 'X', 'H', 'X', 'X', 'X',
  'X', 'X', 'X', 'X', 'X', 'E', 'X', 'G', 'X', 'X', 'X', 'X'
};

const int Engine::number_matrix[] = {
  0, 0, 0, 0, 0, 10, 0, 11, 0, 0, 0, 0,
  0, 0, 0, 0, 9, 0, 10, 0, 11, 0, 0, 0,
  0, 0, 0, 8, 0, 9, 0, 10, 0, 11, 0, 0,
  0, 0, 7, 0, 8, 0, 9, 0, 10, 0, 11, 0,
  0, 0, 0, 7, 0, 8, 0, 9, 0, 10, 0, 0,
  0, 0, 6, 0, 7, 0, 8, 0, 9, 0, 10, 0,
  0, 5, 0, 6, 0, 7, 0, 8, 0, 9, 0, 10,
  0, 0, 5, 0, 6, 0, 7, 0, 8, 0, 9, 0,
  0, 4, 0, 5, 0, 6, 0, 7, 0, 8, 0, 9,
  0, 0, 4, 0, 5, 0, 6, 0, 7, 0, 8, 0,
  0, 3, 0, 4, 0, 5, 0, 6, 0, 7, 0, 8,
  0, 0, 3, 0, 4, 0, 5, 0, 6, 0, 7, 0,
  0, 2, 0, 3, 0, 4, 0, 5, 0, 6, 0, 7,
  0, 0, 2, 0, 3, 0, 4, 0, 5, 0, 6, 0,
  0, 0, 0, 2, 0, 3, 0, 4, 0, 5, 0, 0,
  0, 0, 1, 0, 2, 0, 3, 0, 4, 0, 5, 0,
  0, 0, 0, 1, 0, 2, 0, 3, 0, 4, 0, 0,
  0, 0, 0, 0, 1, 0, 2, 0, 3, 0, 0, 0,
  0, 0, 0, 0, 0, 1, 0, 2, 0, 0, 0, 0
};
const std::string Engine::GAME_NAME = "yinsh";

Engine::Engine(int type, int color) : _type(type) {
  _current_color = Color(color);
  _marker_number = 51;
  _removed_black_ring_number = 0;
  _removed_white_ring_number = 0;
  _intersections = {};
  _phase = Phase::values::PUT_RING;

  for (unsigned int i = 0; i < letters.size(); ++i) {
    const char &l = letters[i];

    for (unsigned int n = begin_number[l - 'A']; n <= end_number[l - 'A']; ++n) {
      Coordinates coordinates(l, n);

      _intersections.insert({coordinates.hash(), Intersection(coordinates)});
    }
  }
}

// public methods
Engine *Engine::clone() const {
  auto *e = new Engine(_type, _current_color);

  e->_phase = _phase;
  e->_marker_number = _marker_number;
  e->_placed_black_ring_coordinates = _placed_black_ring_coordinates;
  e->_placed_white_ring_coordinates = _placed_white_ring_coordinates;
  e->_removed_black_ring_number = _removed_black_ring_number;
  e->_removed_white_ring_number = _removed_white_ring_number;
  e->_intersections = {};
  for (const auto &i: _intersections) {
    int index = i.first;

    e->_intersections.insert({index, i.second});
  }
  e->_last_marker = _last_marker;
  return e;
}

int Engine::current_color() const {
  return _current_color;
}

openxum::core::common::Moves<Decision> Engine::get_possible_move_list() const {
  openxum::core::common::Moves<Decision> moves;

  if (_phase == Phase::PUT_RING) {
    const std::vector<Coordinates> intersections = get_free_intersections();

    for (const auto &e: intersections) {
      moves.push_back(Decision(DecisionType::values::PUT_RING, _current_color, e));
    }
  } else if (_phase == Phase::PUT_MARKER) {
    for (const auto &e: _intersections) {
      const Intersection &intersection = e.second;

      if ((_current_color == Color::BLACK and intersection.state() == State::BLACK_RING) or
          (_current_color == Color::WHITE and intersection.state() == State::WHITE_RING)) {
        if (not get_possible_moving_list(intersection.coordinates(), _current_color, false).empty()) {
          moves.push_back(Decision(DecisionType::values::PUT_MARKER, _current_color, intersection.coordinates()));
        }
      }
    };
  } else if (_phase == Phase::MOVE_RING) {
    std::vector<Coordinates> list = get_possible_moving_list(_last_marker, _current_color, true);

    for (const auto &e: list) {
      moves.push_back(Decision(DecisionType::MOVE_RING, _current_color, _last_marker, e));
    };
  } else if (_phase == Phase::REMOVE_RING_BEFORE or _phase == Phase::REMOVE_RING_AFTER) {
    const std::vector<Coordinates> &rings =
      _current_color == Color::BLACK ? _placed_black_ring_coordinates : _placed_white_ring_coordinates;

    for (const auto &r: rings) {
      moves.push_back(Decision(DecisionType::REMOVE_RING, _current_color, r));
    };
  } else if (_phase == Phase::REMOVE_ROWS_BEFORE or _phase == Phase::REMOVE_ROWS_AFTER) {
    const Rows rows = get_rows(_current_color);

    for (const auto &r: rows) {
      if (r.size() == 5) {
        moves.push_back(Decision(DecisionType::values::REMOVE_ROW, _current_color, r));
      } else {
        for (unsigned int index = 0; index < r.size() - 5; ++index) {
          Row row;

          for (unsigned int i = index; i < index + 5; ++i) {
            row.push_back(r[i]);
          }
          moves.push_back(Decision(DecisionType::values::REMOVE_ROW, _current_color, row));
        }
      }
    };
  }
  return moves;
}

std::string Engine::hash() const {
  std::string str;

  for(const auto& [_, value]: _intersections) {
    str += value.hash();
  }
  return str;
}

bool Engine::is_finished() const {
  if (_type == GameType::BLITZ) {
    return _removed_black_ring_number == 1 or _removed_white_ring_number == 1 or _marker_number == 0;
  } else { // type = REGULAR
    return _removed_black_ring_number == 3 or _removed_white_ring_number == 3 or _marker_number == 0;
  }
}

void Engine::move(const openxum::core::common::Move<Decision> &move) {
  std::for_each(move.begin(), move.end(), [this](const Decision &m) {
    ++_move_number;
    if (m.type() == DecisionType::PUT_RING) {
      put_ring(m.to(), _current_color);
    } else if (m.type() == DecisionType::PUT_MARKER) {
      put_marker(m.to(), _current_color);
    } else if (m.type() == DecisionType::REMOVE_RING) {
      remove_ring(m.to(), _current_color);
    } else if (m.type() == DecisionType::MOVE_RING) {
      move_ring(m.from(), m.to());
    } else if (m.type() == DecisionType::REMOVE_ROW) {
      remove_row(m.row(), _current_color);
    }
  });
}

std::string Engine::to_string() const {
  std::string str("                       10        11\n");
  unsigned int k = 9;

  for (int j = 0; j < 19; ++j) {
    for (int i = 0; i < 12; ++i) {
      char l = letter_matrix[i + 12 * j];
      int n = number_matrix[i + 12 * j];

      if (l != 'X' and l != 'Z') {
        Coordinates coordinates(l, n);
        auto it = _intersections.find(coordinates.hash());

        if (it != _intersections.end()) {
          str += it->second.to_string();
        }
      } else if (l == 'Z') {
        std::string s("    ");

        s += '0' + k;
        str += s;
        --k;
      } else {
        str += "     ";
      }
    }
    str += "\n";
  }
  str += "      A    B    C    D    E    F    G    H    I    J    K\n";
  return str;
}

int Engine::winner_is() const {
  if (is_finished()) {
    if (_type == GameType::REGULAR) {
      if (_removed_black_ring_number == 3 or
          _removed_black_ring_number > _removed_white_ring_number) {
        return Color::BLACK;
      } else if (_removed_white_ring_number == 3 or
                 _removed_black_ring_number < _removed_white_ring_number) {
        return Color::WHITE;
      } else {
        return Color::NONE;
      }
    } else {
      if (_removed_black_ring_number == 1) {
        return Color::BLACK;
      } else if (_removed_white_ring_number == 1) {
        return Color::WHITE;
      } else {
        return Color::NONE;
      }
    }
  }
  return Color::NONE;
}

// private methods
void Engine::build_row(char letter, int number, State state, bool &start,
                       Rows &rows, Row &row) const {
  Coordinates coordinates(letter, number);
  auto intersection = _intersections.find(coordinates.hash());

  if (not start and intersection->second.state() == state) {
    start = true;
    row.push_back(coordinates);
  } else if (start and intersection->second.state() == state) {
    row.push_back(coordinates);
  } else if (start and intersection->second.state() != state) {
    if (row.size() >= 5) {
      rows.push_back(row);
    }
    start = false;
    row.clear();
  }
}

void Engine::change_color() {
  _current_color = _current_color == Color::BLACK ? Color::WHITE : Color::BLACK;
}

void Engine::flip(char letter, int number) {
  Coordinates coordinates(letter, number);

  _intersections.find(coordinates.hash())->second.flip();
}

void Engine::flip(const Intersection &origin, const Intersection &destination) {
  if (origin.letter() == destination.letter()) {
    if (origin.number() < destination.number()) {
      int n = origin.number() + 1;

      while (n < destination.number()) {
        flip(origin.letter(), n);
        ++n;
      }
    } else {
      int n = origin.number() - 1;

      while (n > destination.number()) {
        flip(origin.letter(), n);
        --n;
      }
    }
  } else if (origin.number() == destination.number()) {
    if (origin.letter() < destination.letter()) {
      char l = origin.letter() + 1;

      while (l < destination.letter()) {
        flip(l, origin.number());
        ++l;
      }
    } else {
      char l = origin.letter() - 1;

      while (l > destination.letter()) {
        flip(l, origin.number());
        --l;
      }
    }
  } else {
    if (origin.letter() < destination.letter()) {
      int n = origin.number() + 1;
      char l = origin.letter() + 1;

      while (l < destination.letter()) {
        flip(l, n);
        ++l;
        ++n;
      }
    } else {
      int n = origin.number() - 1;
      char l = origin.letter() - 1;

      while (l > destination.letter()) {
        flip(l, n);
        --l;
        --n;
      }
    }
  }
}

std::vector<Coordinates> Engine::get_free_intersections() const {
  std::vector<Coordinates> list;

  for (char l: letters) {
    for (unsigned int n = begin_number[l - 'A']; n <= end_number[l - 'A']; ++n) {
      Coordinates coordinates(l, n);

      if (_intersections.at(coordinates.hash()).state() == State::VACANT) {
        list.push_back(coordinates);
      }
    }
  }
  return list;
}

void Engine::move_ring(const Coordinates &origin, const Coordinates &destination) {
  auto ito = _intersections.find(origin.hash());
  auto itd = _intersections.find(destination.hash());
  Color color = ito->second.color();

  ito->second.remove_ring();
  itd->second.put_ring(color);
  flip(ito->second, itd->second);

  if (color == Color::BLACK) {
    remove_black_ring(origin);
    _placed_black_ring_coordinates.push_back(destination);
  } else {
    remove_white_ring(origin);
    _placed_white_ring_coordinates.push_back(destination);
  }
  if (not get_rows(_current_color).empty()) {
    _phase = Phase::values::REMOVE_ROWS_AFTER;
  } else {
    change_color();
    if (get_rows(_current_color).empty()) {
      _phase = Phase::values::PUT_MARKER;
    } else {
      _phase = Phase::values::REMOVE_ROWS_BEFORE;
    }
  }
}

std::vector<Coordinates> Engine::get_possible_moving_list(const Coordinates &origin, Color color, bool control) const {
  std::vector<Coordinates> list;
  auto it = _intersections.find(origin.hash());

  if (it == _intersections.end()) {
    return {};
  }

  if (control and not((it->second.state() == BLACK_MARKER_RING and color == BLACK) or
                      (it->second.state() == WHITE_MARKER_RING and color == WHITE))) {
    return {};
  }

  // letter + number increase
  {
    bool ok = true;
    int n = it->second.number() + 1;
    bool no_vacant = false;

    while (n <= end_number[it->second.letter() - 'A'] and ok) {
      verify_intersection(it->second.letter(), n, ok, no_vacant);
      if ((ok and not no_vacant) or (not ok and no_vacant)) {
        list.emplace_back(it->second.letter(), n);
      }
      ++n;
    }
  }
  // letter + number decrease
  {
    bool ok = true;
    int n = it->second.number() - 1;
    bool no_vacant = false;

    while (n >= begin_number[it->second.letter() - 'A'] and ok) {
      verify_intersection(it->second.letter(), n, ok, no_vacant);
      if ((ok and not no_vacant) or (not ok and no_vacant)) {
        list.emplace_back(it->second.letter(), n);
      }
      --n;
    }
  }
  // number + letter increase
  {
    bool ok = true;
    char l = it->second.letter() + 1;
    bool no_vacant = false;

    while (l <= end_letter[it->second.number() - 1] and ok) {
      verify_intersection(l, it->second.number(), ok, no_vacant);
      if ((ok and not no_vacant) or (not ok and no_vacant)) {
        list.emplace_back(l, it->second.number());
      }
      ++l;
    }
  }
  // number + letter decrease
  {
    bool ok = true;
    char l = it->second.letter() - 1;
    bool no_vacant = false;

    while (l >= begin_letter[it->second.number() - 1] and ok) {
      verify_intersection(l, it->second.number(), ok, no_vacant);
      if ((ok and not no_vacant) or (not ok and no_vacant)) {
        list.emplace_back(l, it->second.number());
      }
      --l;
    }
  }
  // number increase + letter increase
  {
    bool ok = true;
    int n = it->second.number() + 1;
    char l = it->second.letter() + 1;
    bool no_vacant = false;

    while (n <= end_number[l - 'A'] and l <= end_letter[n - 1] and ok) {
      verify_intersection(l, n, ok, no_vacant);
      if ((ok and not no_vacant) or (not ok and no_vacant)) {
        list.emplace_back(l, n);
      }
      ++l;
      ++n;
    }
  }
  // number decrease + letter decrease
  {
    bool ok = true;
    int n = it->second.number() - 1;
    char l = it->second.letter() - 1;
    bool no_vacant = false;

    while (n >= begin_number[l - 'A'] and l >= begin_letter[n - 1] and ok) {
      verify_intersection(l, n, ok, no_vacant);
      if ((ok and not no_vacant) or (not ok and no_vacant)) {
        list.emplace_back(l, n);
      }
      --l;
      --n;
    }
  }
  return list;
}

Rows Engine::get_rows(const Color &color) const {
  Rows rows;
  State state = (color == BLACK) ? BLACK_MARKER : WHITE_MARKER;

  for (int n = 1; n <= 11; ++n) {
    bool start = false;
    Row row;

    for (char l = begin_letter[n - 1]; l <= end_letter[n - 1]; ++l) {
      build_row(l, n, state, start, rows, row);
    }
    if (row.size() >= 5) {
      rows.push_back(row);
    }
  }

  for (char l = 'A'; l <= 'K'; ++l) {
    bool start = false;
    Row row;

    for (int n = begin_number[l - 'A']; n <= end_number[l - 'A']; ++n) {
      build_row(l, n, state, start, rows, row);
    }
    if (row.size() >= 5) {
      rows.push_back(row);
    }
  }

  for (int i = 0; i <= 10; ++i) {
    bool start = false;
    Row row;
    int n = begin_diagonal_number[i];
    char l = begin_diagonal_letter[i];

    while (l <= end_diagonal_letter[i] and n <= end_diagonal_number[i]) {
      build_row(l, n, state, start, rows, row);
      ++l;
      ++n;
    }
    if (row.size() >= 5) {
      rows.push_back(row);
    }
  }
  return rows;
}

void Engine::put_marker(const Coordinates &coordinates, const Color &color) {
  _intersections.find(coordinates.hash())->second.put_marker(color);
  _last_marker = coordinates;
  --_marker_number;
  _phase = Phase::MOVE_RING;
}

void Engine::put_ring(const Coordinates &coordinates, const Color &color) {
  _intersections.find(coordinates.hash())->second.put_ring(color);
  if (color == Color::BLACK) {
    _placed_black_ring_coordinates.push_back(coordinates);
  } else {
    _placed_white_ring_coordinates.push_back(coordinates);
  }
  if (_placed_black_ring_coordinates.size() == 5 and
      _placed_white_ring_coordinates.size() == 5) {
    _phase = Phase::PUT_MARKER;
  }
  change_color();
}

void Engine::remove_black_ring(const Coordinates &coordinates) {
  auto it = _placed_black_ring_coordinates.begin();
  bool found = false;

  while (it != _placed_black_ring_coordinates.end() and not found) {
    found = *it == coordinates;
    if (not found) {
      ++it;
    }
  }
  if (found) {
    _placed_black_ring_coordinates.erase(it);
  }
}

void Engine::remove_marker(char letter, int number) {
  Coordinates coordinates(letter, number);

  _intersections.find(coordinates.hash())->second.remove_marker();
  ++_marker_number;
}

void Engine::remove_ring(const Coordinates &coordinates, const Color &color) {
  _intersections.find(coordinates.hash())->second.remove_ring_board();
  if (color == Color::BLACK) {
    remove_black_ring(coordinates);
    ++_removed_black_ring_number;
  } else {
    remove_white_ring(coordinates);
    ++_removed_white_ring_number;
  }
  if (not get_rows(_current_color).empty()) {
    _phase = Phase::values::REMOVE_ROWS_AFTER;
  } else {
    change_color();
    if (get_rows(_current_color).empty()) {
      _phase = Phase::values::PUT_MARKER;
    } else {
      _phase = Phase::values::REMOVE_ROWS_BEFORE;
    }
  }
}

void Engine::remove_row(const Row &row, const Color &color) {
  for (const auto &e: row) {
    remove_marker(e.letter(), e.number());
  }
  if (_phase == Phase::values::REMOVE_ROWS_AFTER) {
    _phase = Phase::values::REMOVE_RING_AFTER;
  } else {
    _phase = Phase::values::REMOVE_RING_BEFORE;
  }
}

void Engine::remove_white_ring(const Coordinates &coordinates) {
  auto it = _placed_white_ring_coordinates.begin();
  bool found = false;

  while (it != _placed_white_ring_coordinates.end() and not found) {
    found = *it == coordinates;
    if (not found) {
      ++it;
    }
  }
  if (found) {
    _placed_white_ring_coordinates.erase(it);
  }
}

void Engine::verify_intersection(char letter, int number, bool &ok, bool &no_vacant) const {
  Coordinates coordinates(letter, number);
  auto it = _intersections.find(coordinates.hash());

  if (it != _intersections.end()) {
    State state = it->second.state();

    if (state == BLACK_RING or state == WHITE_RING) {
      no_vacant = false; // if ring is presenter after row of markers
      ok = false;
    } else if (state == BLACK_MARKER or state == WHITE_MARKER) {
      no_vacant = true;
    } else if (state == VACANT and no_vacant) {
      ok = false;
    }
  }
}


}