/**
 * @file openxum/core/games/yinsh/row.hpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2024 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPENXUM_CORE_GAMES_YINSH_ROW_HPP
#define OPENXUM_CORE_GAMES_YINSH_ROW_HPP

#include <string>
#include <vector>

#include "coordinates.hpp"

namespace openxum::core::games::yinsh {

class Row : public std::vector<Coordinates> {
public:
  Row() = default;

  Row(const Coordinates &begin, const Coordinates &end);

  virtual ~Row() = default;

  bool belong(const Coordinates &coordinates) const;

  bool is_separated(const Row &row) const;

  std::string to_string() const;

private:
  void build(const Coordinates &begin, const Coordinates &end);
};

using Rows = std::vector<Row>;

using SeparatedRows = std::vector<Rows>;

}

#endif
