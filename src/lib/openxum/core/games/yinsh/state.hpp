/**
 * @file openxum/core/games/yinsh/state.hpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2024 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPENXUM_CORE_GAMES_YINSH_STATE_HPP
#define OPENXUM_CORE_GAMES_YINSH_STATE_HPP

namespace openxum::core::games::yinsh {

enum State {
  VACANT = 0, BLACK_MARKER = 1, WHITE_MARKER = 2, BLACK_RING = 3, WHITE_RING = 4,
  BLACK_MARKER_RING = 5, WHITE_MARKER_RING = 6
};

}

#endif