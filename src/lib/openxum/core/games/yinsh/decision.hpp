/**
 * @file openxum/core/games/yinsh/decision.hpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2024 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPENXUM_CORE_GAMES_YINSH_DECISION_HPP
#define OPENXUM_CORE_GAMES_YINSH_DECISION_HPP

#include <openxum/core/common/decision.hpp>

#include <openxum/core/games/yinsh/coordinates.hpp>
#include <openxum/core/games/yinsh/color.hpp>
#include <openxum/core/games/yinsh/decision_type.hpp>
#include <openxum/core/games/yinsh/row.hpp>

namespace openxum::core::games::yinsh {

class Decision : public openxum::core::common::Decision {
public:
  Decision() = default;

  Decision(const DecisionType::values &type, const Color &color);

  Decision(const DecisionType::values &type, const Color &color, const Coordinates &to);

  Decision(const DecisionType::values &type, const Color &color, const Row &row);

  Decision(const DecisionType::values &type, const Color &color, const Coordinates &from, const Coordinates &to);

  Color color() const { return _color; }

  void decode(const std::string &) override;

  std::string encode() const override;

  const Coordinates &from() const { return _from; }

  void from_object(const nlohmann::json &) override;

  const Row &row() const { return _row; }

  const Coordinates &to() const { return _to; }

  nlohmann::json to_object() const override;

  std::string to_string() const override;

  const DecisionType::values &type() const { return _type; }

private:
  DecisionType::values _type;
  Color _color;
  Coordinates _from;
  Coordinates _to;
  Row _row;
};

}

#endif