/**
 * @file openxum/core/games/yinsh/coordinates.hpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2024 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPENXUM_CORE_GAMES_YINSH_COORDINATES_HPP
#define OPENXUM_CORE_GAMES_YINSH_COORDINATES_HPP

#include <nlohmann/json.hpp>
#include <string>

namespace openxum::core::games::yinsh {

class Coordinates {
public:
  Coordinates(char l = ' ', int n = -1) : _letter(l), _number(n) {}

  Coordinates(const Coordinates &other) : _letter(other._letter), _number(other._number) {}

  void from_object(const nlohmann::json &json) {
    _letter = json["letter"].get<char>();
    _number = json["number"].get<int>();
  }

  int hash() const {
    return (_letter - 'A') + (_number - 1) * 11;
  }

  bool is_valid() const {
    return (_letter == 'A' and _number >= 2 and _number <= 5) or
           (_letter == 'B' and _number >= 1 and _number <= 7) or
           (_letter == 'C' and _number >= 1 and _number <= 8) or
           (_letter == 'D' and _number >= 1 and _number <= 9) or
           (_letter == 'E' and _number >= 1 and _number <= 10) or
           (_letter == 'F' and _number >= 2 and _number <= 10) or
           (_letter == 'G' and _number >= 2 and _number <= 11) or
           (_letter == 'H' and _number >= 3 and _number <= 11) or
           (_letter == 'I' and _number >= 4 and _number <= 11) or
           (_letter == 'J' and _number >= 5 and _number <= 11) or
           (_letter == 'K' and _number >= 7 and _number <= 10);
  }

  bool operator==(const Coordinates &other) const { return _letter == other._letter and _number == other._number; }

  nlohmann::json to_object() const {
    nlohmann::json json;

    json["letter"] = _letter;
    json["number"] = _number;
    return json;
  }

  std::string to_string() const { return _letter + std::to_string(_number); }

  char letter() const { return _letter; }

  int number() const { return _number; }

private:
  char _letter;
  int _number;
};

}

#endif