/**
 * @file openxum/core/games/yinsh/intersection.hpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2024 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPENXUM_CORE_GAMES_YINSH_INTERSECTION_HPP
#define OPENXUM_CORE_GAMES_YINSH_INTERSECTION_HPP

#include <openxum/core/games/yinsh/color.hpp>
#include <openxum/core/games/yinsh/coordinates.hpp>
#include <openxum/core/games/yinsh/state.hpp>

namespace openxum::core::games::yinsh {

struct Intersection {
  Intersection(const Coordinates &coordinates) : _coordinates(coordinates), _state(State::VACANT) {}

  Intersection(const Intersection& other) : _coordinates(other.coordinates()), _state(other.state()) {}

  Color color() const {
    if (_state == State::VACANT) {
      return Color::NONE;
    }

    if (_state == State::BLACK_RING or _state == State::BLACK_MARKER or _state == State::BLACK_MARKER_RING) {
      return Color::BLACK;
    } else {
      return Color::WHITE;
    }
  }

  const Coordinates &coordinates() const {
    return _coordinates;
  }

  void flip() {
    if (_state == State::BLACK_MARKER) {
      _state = State::WHITE_MARKER;
    } else if (_state == State::WHITE_MARKER) {
      _state = State::BLACK_MARKER;
    }
  }

  std::string hash() const {
    switch (_state) {
      case BLACK_MARKER:
        return "BM";
      case WHITE_MARKER:
        return "WM";
      case BLACK_RING:
        return "BR";
      case WHITE_RING:
        return "WR";
      case BLACK_MARKER_RING:
        return "BMR";
      case WHITE_MARKER_RING:
        return "WMR";
      default:
        return "X";
    }
  }

  char letter() const {
    return _coordinates.letter();
  }

  int number() const {
    return _coordinates.number();
  }

  void put_marker(const Color &color) {
    if (color == Color::BLACK) {
      if (_state == State::BLACK_RING) {
        _state = State::BLACK_MARKER_RING;
      }
    } else {
      if (_state == State::WHITE_RING) {
        _state = State::WHITE_MARKER_RING;
      }
    }
  }

  void put_ring(const Color &color) {
    if (color == Color::BLACK) {
      _state = State::BLACK_RING;
    } else {
      _state = State::WHITE_RING;
    }
  }

  void remove_marker() {
    _state = State::VACANT;
  }

  void remove_ring() {
    if (_state == State::BLACK_MARKER_RING or _state == State::WHITE_MARKER_RING) {
      if (_state == State::BLACK_MARKER_RING) {
        _state = State::BLACK_MARKER;
      } else {
        _state = State::WHITE_MARKER;
      }
    }
  }

  void remove_ring_board() {
    _state = State::VACANT;
  }

  const State &state() const {
    return _state;
  }

//  void set_state(State state) {
//    _state = state;
//  }

  std::string to_string() const {
    switch (_state) {
      case BLACK_MARKER:
        return "[BM ]";
      case WHITE_MARKER:
        return "[WM ]";
      case BLACK_RING:
        return "[BR ]";
      case WHITE_RING:
        return "[WR ]";
      case BLACK_MARKER_RING:
        return "[BMR]";
      case WHITE_MARKER_RING:
        return "[WMR]";
      default:
        return "[   ]";
    }
  }

  Coordinates _coordinates;
  State _state;
};

}

#endif //OPENXUM_CORE_GAMES_YINSH_INTERSECTION_HPP
