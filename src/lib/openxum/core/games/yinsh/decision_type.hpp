/**
 * @file openxum/core/games/yinsh/decision_type.hpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2024 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPENXUM_CORE_GAMES_YINSH_DECISION_TYPE_HPP
#define OPENXUM_CORE_GAMES_YINSH_DECISION_TYPE_HPP

#include <openxum/core/common/decision.hpp>

#include <openxum/core/games/komivoki/coordinates.hpp>

namespace openxum::core::games::yinsh {

struct DecisionType {
  enum values {
    PUT_RING = 0, PUT_MARKER = 1, MOVE_RING = 2, REMOVE_ROW = 3, REMOVE_RING = 4
  };
};

}

#endif