/**
 * @file openxum/core/games/komivoki/board_initializer.hpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2024 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPENXUM_WS_CPP_BOARD_INITIALIZER_HPP
#define OPENXUM_WS_CPP_BOARD_INITIALIZER_HPP

#include <utility>
#include <vector>

namespace openxum::core::games::komivoki {

class Engine;

struct BoardInitializer {
  virtual void operator()(Engine & /* engine */) const {}
};

struct FixedBoardInitializer : public komivoki::BoardInitializer {

  using Configuration = std::vector<std::vector<std::pair<int, int>>>;

  FixedBoardInitializer(Configuration configuration) : _configuration(std::move(configuration)) {}

  void operator()(komivoki::Engine &engine) const override;

  const Configuration _configuration;
};

struct TwoCompleteLineBoardInitializer : public komivoki::BoardInitializer {
  void operator()(komivoki::Engine &engine) const override;
};

struct CornerBoardInitializer : public komivoki::BoardInitializer {
  CornerBoardInitializer(int level) : _level(level) {}

  void operator()(komivoki::Engine &engine) const override;

  const int _level;
};

struct RandomBoardInitializer : public komivoki::BoardInitializer {
  RandomBoardInitializer(unsigned int entity_number, const std::pair<int, int> &level,
                         unsigned int seed) : _entity_number(entity_number), _level(level), _seed(seed) {}

  void operator()(komivoki::Engine &engine) const override;

  const unsigned int _entity_number;
  const std::pair<int, int> _level;
  const unsigned int _seed;
};

}

#endif //OPENXUM_WS_CPP_BOARD_INITIALIZER_HPP
