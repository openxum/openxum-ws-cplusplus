/**
 * @file openxum/core/games/komivoki/engine.cpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2024 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <openxum/core/games/komivoki/engine.hpp>

#include <cassert>
#include <memory>
#include <stack>

namespace openxum::core::games::komivoki {

const std::string Engine::GAME_NAME = "komivoki";

// constructors
Engine::Engine(int type, int color, const std::pair<int, int> &size, unsigned int max_length,
               const BoardInitializer &init, const Deltas &deltas)
  : _type(type), _size(size), _max_length(max_length), _color(Color(color)), _phase(MOVE_ENTITY),
    _black_entity_number(0), _white_entity_number(0), _pass(0), _deltas(deltas) {
  for (int x = 0; x < _size.first; ++x) {
    for (int y = 0; y < _size.second; ++y) {
      _board.emplace_back();
    }
  }
  init(*this);
  _ids.emplace_back();
  _ids.emplace_back();
  _ids[Color::BLACK].push_back(hash(Color::BLACK));
  _ids[Color::WHITE].push_back(hash(Color::WHITE));
}

// public methods
Engine *Engine::clone() const {
  auto e = new Engine(_type, _color, _size, _max_length, BoardInitializer());

  for (int x = 0; x < _size.first; ++x) {
    for (int y = 0; y < _size.second; ++y) {
      if (not _board[x + _size.first * y].empty()) {
        for (const auto &entity: _board[x + _size.first * y]) {
          e->_board[x + _size.first * y].push_back(std::make_shared<Entity>(entity->color, entity->level));
        }
        std::reverse(e->_board[x + _size.first * y].begin(), e->_board[x + _size.first * y].end());
      }
    }
  }
  e->_phase = _phase;
  e->_black_entity_number = _black_entity_number;
  e->_white_entity_number = _white_entity_number;
  e->_pass = _pass;
  e->_ids = _ids;
  return e;
}

int Engine::current_color() const {
  return _color;
}

double Engine::gain(int color, bool finish) const {
  if (color == Color::BLACK) {
    int gain = _black_entity_number - _white_entity_number;

    return gain > 0 ? gain : 0;
  } else {
    int gain = _white_entity_number - _black_entity_number;

    return gain > 0 ? gain : 0;
  }
}

openxum::core::common::Moves<Decision> Engine::get_possible_move_list() const {
  openxum::core::common::Moves<Decision> moves;

  if (_phase == MOVE_ENTITY) {
    for (int x = 0; x < _size.first; ++x) {
      for (int y = 0; y < _size.second; ++y) {
        if (not _board[x + _size.first * y].empty() and _board[x + _size.first * y].cbegin()->get()->color == _color) {
          Coordinates from(x, y);

          std::for_each(_deltas.cbegin(), _deltas.cend(), [this, x, y, from, &moves](const auto &p) {
            Coordinates to(x + p.first, y + p.second);

            if (is_possible_cell(Color(current_color()), from, to)) {
              if (is_new_state(Color(current_color()), from, to)) {
                moves.push_back(common::Move<Decision>(Decision(MOVE, Color(current_color()), from, to)));
              }
            }
          });
        }
      }
    }
  } else if (_phase == CAPTURE_ENTITY_BEFORE or _phase == CAPTURE_ENTITY_AFTER) {
    for (int x = 0; x < _size.first; ++x) {
      for (int y = 0; y < _size.second; ++y) {
        Coordinates from(x, y);

        if (is_possible_to_capture(Color(_color), from)) {
          moves.push_back(common::Move<Decision>(Decision(CAPTURE, Color(current_color()), from)));
        }
      }
    }
  }
  if (moves.empty()) {
    moves.push_back(common::Move<Decision>(Decision(PASS, Color(current_color()))));
  }
  return moves;
}

std::string Engine::hash() const {
  return hash(this->current_color());
}

std::string Engine::hash(int color) const {
  std::string str;

  for (int x = 0; x < _size.first; ++x) {
    for (int y = 0; y < _size.second; ++y) {
      const auto &cell = _board[x + _size.first * y];

      if (cell.empty()) {
        str += "X";
      } else if (cell.cbegin()->get()->color == color) {
        str += cell.cbegin()->get()->color == BLACK ? "B" : "W";
        for (const auto &e: cell) {
          str += std::to_string(e->level);
        }
      } else {
        str += "X";
      }
    }
  }
  return str;
}

bool Engine::is_finished() const {
  return _phase == FINISH;
}

void Engine::move(const openxum::core::common::Move<Decision> &move) {
  std::for_each(move.begin(), move.end(), [this](const Decision &m) {
                  ++_move_number;
                  if (m.type() == MOVE) {
                    _pass = 0;
                    if ((_type & 0x0F00) == GameType::VARIANT_MOVE_TOP_ENTITY) {
                      _board[m.to().x() + _size.first * m.to().y()].push_back(
                        _board[m.from().x() + _size.first * m.from().y()].back());
                      _board[m.from().x() + _size.first * m.from().y()].pop_back();
                    } else if ((_type & 0x0F00) == GameType::VARIANT_MOVE_STACK) {
                      Stack& moved_stack = _board[m.from().x() + _size.first * m.from().y()];
                      Stack& new_stack = _board[m.to().x() + _size.first * m.to().y()];
                      Stack temporary_stack;

                      while (not moved_stack.empty()) {
                        temporary_stack.push_back(moved_stack.back());
                        moved_stack.pop_back();
                      }
                      while (not temporary_stack.empty()) {
                        new_stack.push_back(temporary_stack.back());
                        temporary_stack.pop_back();
                      }
                    }
                    if (is_possible_to_capture(Color(current_color()))) {
                      _phase = Phase::CAPTURE_ENTITY_AFTER;
                    } else {
                      change_color();
                      if (is_possible_to_capture(Color(current_color()))) {
                        _phase = Phase::CAPTURE_ENTITY_BEFORE;
                      } else {
                        _phase = Phase::MOVE_ENTITY;
                      }
                    }
                  } else if (m.type() == CAPTURE) {
                    std::vector<std::shared_ptr<Entity>> &cell = _board[m.from().x() + _size.first * m.from().y()];

                    _pass = 0;
                    capture(cell);
                    if ((_color == BLACK and _white_entity_number == 0) or (_color == WHITE and _black_entity_number == 0)) {
                      _phase = Phase::FINISH;
                    } else {
                      if (_phase == CAPTURE_ENTITY_BEFORE) {
                        if (not is_possible_to_capture(Color(current_color()))) {
                          _phase = Phase::MOVE_ENTITY;
                        }
                      } else { // CAPTURE_ENTITY_AFTER
                        if (not is_possible_to_capture(Color(current_color()))) {
                          change_color();
                          if (is_possible_to_capture(Color(current_color()))) {
                            _phase = Phase::CAPTURE_ENTITY_BEFORE;
                          } else {
                            _phase = Phase::MOVE_ENTITY;
                          }
                        }
                      }
                    }
                  } else if (m.type() == PASS) {
                    ++_pass;
                    if (_pass == 2) {
                      _phase = Phase::FINISH;
                    } else {
                      change_color();
                      _phase = Phase::MOVE_ENTITY;
                    }
                  }
                  _ids[current_color() == BLACK ? WHITE : BLACK].push_back(hash(current_color() == BLACK ? WHITE : BLACK));
                }
  );
}

std::string Engine::to_string() const {
  std::string str;

  for (int y = 0; y < _size.second; ++y) {
    for (int x = 0; x < _size.first; ++x) {
      str += "[";
      if (_board[x + _size.first * y].empty()) {
        str += "      ";
      } else {
        const auto &cell = _board[x + _size.first * y];
        unsigned int capacity = std::accumulate(cell.cbegin(), cell.cend(), 0, [](int v, const auto &e) {
          return v + e->level;
        });

        str += cell.cbegin()->get()->color == BLACK ? "B" : "W";
        str += std::to_string(capacity);
        str += "/";
        str += std::to_string(cell.cbegin()->get()->level);
        str += "/";
        str += std::to_string(cell.size());
      }
      str += "]";
    }
    str += "\n";
  }
  return str;
}

int Engine::winner_is() const {
  if (is_finished()) {
    if (_pass == 2) {
      return NONE;
    } else {
      return _color;
    }
  } else {
    return NONE;
  }
}

// private methods
void Engine::capture(std::vector<std::shared_ptr<Entity>> &cell) {
  if ((_type & 0x00F0) == VARIANT_CAPTURE_REMOVE) {
    if (_color == BLACK) {
      _white_entity_number -= cell.size();
    } else {
      _black_entity_number -= cell.size();
    }
    cell.erase(cell.begin(), cell.end());
  } else if ((_type & 0x00F0) == VARIANT_CAPTURE_ENLIST) {
    if (_color == BLACK) {
      _white_entity_number -= cell.size();
      _black_entity_number += cell.size();
    } else {
      _white_entity_number += cell.size();
      _black_entity_number -= cell.size();
    }
    std::for_each(cell.begin(), cell.end(), [](auto &e) { e->switch_color(); });
  }
}

void Engine::change_color() {
  _color = next_color(_color);
}

unsigned int Engine::get_group_number(const std::vector<bool> &matrix) const {
  int group_index = 1;
  std::vector<int> group_matrix(_size.first * _size.second, 0);

  for (int x = 0; x < _size.first; ++x) {
    for (int y = 0; y < _size.second; ++y) {
      if (matrix[x + y * _size.first] and group_matrix[x + _size.first * y] == 0) {
        std::stack<Coordinates> stack;

        stack.emplace(x, y);
        while (not stack.empty()) {
          Coordinates coordinates = stack.top();

          stack.pop();
          group_matrix[coordinates.x() + _size.first * coordinates.y()] = group_index;
          std::for_each(_deltas.cbegin(), _deltas.cend(),
                        [this, matrix, group_matrix, coordinates, &stack](const auto &p) {
                          Coordinates neighbour(coordinates.x() + p.first, coordinates.y() + p.second);

                          if (neighbour.in_range(_size)) {
                            if (matrix[neighbour.x() + neighbour.y() * _size.first] and
                                group_matrix[neighbour.x() + _size.first * neighbour.y()] == 0) {
                              stack.push(neighbour);
                            }
                          }
                        });
        }
        ++group_index;
      }
    }
  }
  --group_index;
  return group_index;
}

bool Engine::is_new_state(const Color &color, const Coordinates &from, const Coordinates &to) const {
  std::string str;

  for (int x = 0; x < _size.first; ++x) {
    for (int y = 0; y < _size.second; ++y) {
      if (from.x() == x and from.y() == y) {
        if (_board[from.x() + _size.first * from.y()].size() == 1) {
          str += "X";
        } else {
          auto it = _board[from.x() + _size.first * from.y()].cbegin();

          str += _board[x + _size.first * y].cbegin()->get()->color == BLACK ? "B" : "W";
          while (it + 1 != _board[from.x() + _size.first * from.y()].cend()) {
            str += std::to_string((*it)->level);
            ++it;
          }
        }
      } else if (to.x() == x and to.y() == y) {
        str += _board[from.x() + _size.first * from.y()].cbegin()->get()->color == BLACK ? "B" : "W";
        for (const auto &e: _board[to.x() + _size.first * to.y()]) {
          str += std::to_string(e->level);
        }
        str += std::to_string(_board[from.x() + _size.first * from.y()].back()->level);
      } else {
        const auto &cell = _board[x + _size.first * y];

        if (cell.empty()) {
          str += "X";
        } else if (cell.cbegin()->get()->color == current_color()) {
          str += cell.cbegin()->get()->color == BLACK ? "B" : "W";
          for (const auto &e: cell) {
            str += std::to_string(e->level);
          }
        } else {
          str += "X";
        }
      }
    }
  }
  return std::find(_ids[color].cbegin(), _ids[color].cend(), str) == _ids[color].cend();
}

bool Engine::is_possible_cell(const Color &color, const Coordinates &from, const Coordinates &to) const {
  if (to.in_range(_size)) {
    const Stack &from_cell = _board[from.x() + _size.first * from.y()];
    const Stack &to_cell = _board[to.x() + _size.first * to.y()];
    bool ok = false;

    if ((_type & 0x0F00) == GameType::VARIANT_MOVE_TOP_ENTITY) {
      ok = to_cell.empty() or (to_cell.back()->color == color and to_cell.size() < _max_length);
    } else if ((_type & 0x0F00) == GameType::VARIANT_MOVE_STACK) {
      ok = to_cell.empty() or (to_cell.back()->color == color and to_cell.size() + from_cell.size() <= _max_length);
    }
    if ((_type & 0x000F) == GameType::VARIANT_NO_CONNECT) {
      return ok;
    } else if ((_type & 0x000F) == GameType::VARIANT_CONNECT) {
      if (ok) {
        std::vector<bool> origin_matrix;

        std::for_each(_board.cbegin(), _board.cend(), [&origin_matrix, color](const auto &cell) {
          if (not cell.empty() and cell.back()->color == color) {
            origin_matrix.push_back(true);
          } else {
            origin_matrix.push_back(false);
          }
        });

        std::vector<bool> modified_matrix;

        for (int y = 0; y < _size.second; ++y) {
          for (int x = 0; x < _size.first; ++x) {
            Coordinates coordinates(x, y);

            if (coordinates == to) {
              modified_matrix.push_back(true);
            } else {
              const auto &cell = _board[x + y * _size.first];

              if (coordinates == from) {
                modified_matrix.push_back(cell.size() > 1);
              } else {
                modified_matrix.push_back(not cell.empty() and cell.back()->color == color);
              }
            }
          }
        }
        if (get_group_number(modified_matrix) <= get_group_number(origin_matrix)) {
          return ok;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } else {
      return false;
    }
  } else {
    return false;
  }
}

bool Engine::is_possible_to_capture(const Color &color) const {
  for (int x = 0; x < _size.first; ++x) {
    for (int y = 0; y < _size.second; ++y) {
      if (is_possible_to_capture(color, Coordinates(x, y))) {
        return true;
      }
    }
  }
  return false;
}

bool Engine::is_possible_to_capture(const Color &color, const Coordinates &coordinates) const {
  const std::vector<std::shared_ptr<Entity>> &cell = _board[coordinates.x() + _size.first * coordinates.y()];

  if (not cell.empty() and cell.cbegin()->get()->color != color) {
    unsigned int sum = 0;
    unsigned int neighbour_sum = 0;
    unsigned int level_sum = 0;
    unsigned int capacity = std::accumulate(cell.cbegin(), cell.cend(), 0, [](int v, const auto &e) {
      return v + e->level;
    });

    std::for_each(_deltas.cbegin(), _deltas.cend(),
                  [this, color, coordinates, &sum, &neighbour_sum, &level_sum](const auto &p) {
                    Coordinates c(coordinates.x() + p.first, coordinates.y() + p.second);

                    if (c.in_range(_size)) {
                      const std::vector<std::shared_ptr<Entity>> &neighbour = _board[c.x() + _size.first * c.y()];

                      if (not neighbour.empty() and neighbour.cbegin()->get()->color == color) {
                        neighbour_sum += neighbour.size();
                        sum += neighbour.size();
                        std::for_each(neighbour.cbegin(), neighbour.cend(), [&level_sum](const auto &e) {
                          level_sum += e->level;
                        });
                      }
                    } else {
                      ++sum;
                    }
                  });
    if (neighbour_sum > 0 and
        ((sum >= cell.size() and level_sum > capacity) or (sum > cell.size() and level_sum == capacity))) {
      return true;
    }
  }
  return false;
}

int Engine::next_color(int color) {
  return color == WHITE ? BLACK : WHITE;
}

void Engine::put_entity(const Coordinates &coordinates, Entity *entity) {
  _board[coordinates.x() + _size.first * coordinates.y()].push_back(
    std::shared_ptr<Entity>(entity));
  if (entity->color == BLACK) {
    ++_black_entity_number;
  } else {
    ++_white_entity_number;
  }
}

}