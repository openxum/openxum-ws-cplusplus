/**
 * @file openxum/core/games/komivoki/engine.hpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2024 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPENXUM_CORE_GAMES_KOMIVOKI_ENGINE_HPP
#define OPENXUM_CORE_GAMES_KOMIVOKI_ENGINE_HPP

#include <openxum/core/common/two_player_engine.hpp>

#include <openxum/core/games/komivoki/board_initializer.hpp>
#include <openxum/core/games/komivoki/color.hpp>
#include <openxum/core/games/komivoki/coordinates.hpp>
#include <openxum/core/games/komivoki/decision.hpp>
#include <openxum/core/games/komivoki/entity.hpp>
#include <openxum/core/games/komivoki/game_type.hpp>
#include <openxum/core/games/komivoki/phase.hpp>

#include <vector>

namespace openxum::core::games::komivoki {

using Deltas = std::vector<std::pair<int, int>>;

const Deltas four_deltas = {{-1, 0},
                            {0,  -1},
                            {0,  1},
                            {1,  0}};

const Deltas eight_deltas = {{-1, -1},
                             {-1, 0},
                             {-1, 1},
                             {0,  -1},
                             {0,  1},
                             {1,  -1},
                             {1,  0},
                             {1,  1}};

class Engine : public openxum::core::common::TwoPlayerEngine<Decision> {

  using Stack = std::vector<std::shared_ptr<Entity>>;
  using Board = std::vector<Stack>;

public:
  Engine() : _type(VARIANT_NO_CONNECT), _color(Color::WHITE), _size({0, 0}), _max_length(0), _deltas(eight_deltas) {}

  Engine(int type, int color, const std::pair<int, int> &size = {8, 8}, unsigned int max_length = 3,
         const BoardInitializer &init = TwoCompleteLineBoardInitializer(),
         const Deltas &deltas = eight_deltas);

  ~Engine() override = default;

  int best_is() const override { return NONE; }

  const Board &board() const { return _board; }

  Engine *clone() const override;

  int current_color() const override;

  unsigned int current_goal(int color) const override {
    return color == Color::BLACK ? _white_entity_number : _black_entity_number;
  }

  double gain(int /* color */, bool /* finish */) const override;

  const std::string &get_name() const override { return GAME_NAME; }

  openxum::core::common::Moves<Decision> get_possible_move_list() const override;

  std::string hash() const override;

  bool is_finished() const override;

  bool is_stoppable() const override { return false; }

  void move(const openxum::core::common::Move<Decision> &move) override;

  void parse(const std::string &) override {}

  void put_entity(const Coordinates &coordinates, Entity *entity);

  const std::pair<int, int> &size() const { return _size; }

  int type() const { return _type; }

  std::string to_string() const override;

  int winner_is() const override;

private:
  void capture(std::vector<std::shared_ptr<Entity>> &cell);

  void change_color();

  unsigned int get_group_number(const std::vector<bool> &matrix) const;

  std::string hash(int color) const;

  bool is_new_state(const Color &color, const Coordinates &from, const Coordinates &to) const;

  bool is_possible_cell(const Color &color, const Coordinates &from, const Coordinates &to) const;

  bool is_possible_to_capture(const Color &color) const;

  bool is_possible_to_capture(const Color &color, const Coordinates &coordinates) const;

  int next_color(int color);

  const int _type;
  const std::pair<int, int> _size;
  const unsigned int _max_length;
  const Deltas &_deltas;

  int _color{};
  int _phase{};
  Board _board;
  unsigned int _black_entity_number{};
  unsigned int _white_entity_number{};
  std::vector<std::vector<std::string>> _ids;
  unsigned int _pass{};

  const static std::string GAME_NAME;
};

}

#endif
