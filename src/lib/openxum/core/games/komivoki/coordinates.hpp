/**
 * @file openxum/core/games/komivoki/coordinates.hpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2024 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPENXUM_CORE_GAMES_KOMIVOKI_COORDINATES_HPP
#define OPENXUM_CORE_GAMES_KOMIVOKI_COORDINATES_HPP

#include <nlohmann/json.hpp>
#include <string>

namespace openxum::core::games::komivoki {

class Coordinates {
public:
  explicit Coordinates(int = -1, int = -1);

  void from_object(const nlohmann::json &json) {
    _x = json["x"].get<int>();
    _y = json["y"].get<int>();
  }

  bool in_range(const std::pair<int, int> &size) const {
    return _x >= 0 and _x < size.first and _y >= 0 and _y < size.second;
  }

  bool is_valid() const { return _x != -1 and _y != -1; }

  bool operator==(const Coordinates& other) const { return _x == other._x and _y == other._y; }

  nlohmann::json to_object() const {
    nlohmann::json json;

    json["x"] = _x;
    json["y"] = _y;
    return json;
  }

  std::string to_string() const { return char('a' + _x) + std::to_string(_y + 1); }

  int x() const { return _x; }

  int y() const { return _y; }

private:
  int _x;
  int _y;
};

}

#endif