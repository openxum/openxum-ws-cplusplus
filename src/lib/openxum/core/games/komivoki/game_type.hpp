/**
 * @file openxum/core/games/komivoki/game_type.hpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2024 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPENXUM_CORE_GAMES_KOMIVOKI_GAME_TYPE_HPP
#define OPENXUM_CORE_GAMES_KOMIVOKI_GAME_TYPE_HPP

namespace openxum::core::games::komivoki {

enum GameType {
  VARIANT_NO_CONNECT = 0x0001,
  VARIANT_CONNECT = 0x0002, // an entity must have a neighbour entity with same color
  VARIANT_CAPTURE_REMOVE = 0x0010,
  VARIANT_CAPTURE_ENLIST = 0x0020,
  VARIANT_MOVE_TOP_ENTITY = 0x0100,
  VARIANT_MOVE_STACK = 0x0200
};

}

#endif