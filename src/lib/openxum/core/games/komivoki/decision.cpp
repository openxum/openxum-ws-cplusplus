/**
 * @file openxum/core/games/komivoki/decision.cpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2024 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <openxum/core/games/komivoki/decision.hpp>

namespace openxum::core::games::komivoki {

Decision::Decision(const DecisionType &type, const Color &color)
  : _type(type), _color(color) {}

Decision::Decision(const DecisionType &type, const Color &color, const Coordinates &from)
  : _type(type), _color(color), _from(from) {}

Decision::Decision(const DecisionType &type, const Color &color, const Coordinates &from, const Coordinates &to)
  : _type(type), _color(color), _from(from), _to(to) {}

void Decision::decode(const std::string &str) {
  if (str[0] == 'M') {
    _type = MOVE;
  } else if (str[0] == 'C') {
    _type = CAPTURE;
  } else {
    _type = PASS;
  }
  _color = str[1] == 'B' ? BLACK : WHITE;
  if (_type == MOVE or _type == CAPTURE) {
    _from = Coordinates(str[2] - 'a', str[3] - '1');
    if (_type == MOVE) {
      _to = Coordinates(str[4] - 'a', str[5] - '1');
    }
  }
}

std::string Decision::encode() const {
  if (_type == MOVE) {
    return "M" + std::string(_color == BLACK ? "B" : "W") + _from.to_string() + _to.to_string();
  } else if (_type == CAPTURE) {
    return "C" + std::string(_color == BLACK ? "B" : "W") + _from.to_string();
  } else {
    return "P" + std::string(_color == BLACK ? "B" : "W");
  }
}

void Decision::from_object(const nlohmann::json &json) {
  _type = DecisionType(json["type"].get<int>());
  _color = DecisionType(json["color"].get<int>()) == 0 ? BLACK : WHITE;
  if (_type == MOVE) {
    Coordinates from, to;

    from.from_object(json["from"]);
    to.from_object(json["to"]);
    _from = from;
    _to = to;
  } else if (_type == CAPTURE) {
    Coordinates from;

    from.from_object(json["from"]);
    _from = from;
  }
}

nlohmann::json Decision::to_object() const {
  nlohmann::json json;

  json["type"] = _type;
  json["color"] = _color;
  if (_type == MOVE) {
    json["from"] = _from.to_object();
    json["to"] = _to.to_object();
  } else if (_type == CAPTURE) {
    json["from"] = _from.to_object();
  }
  return json;
}

std::string Decision::to_string() const {
  if (_type == MOVE) {
    return "move " + std::string(_color == BLACK ? "black" : "white") + " entity from " + _from.to_string() + " to " +
           _to.to_string();
  } else if (_type == CAPTURE) {
    return "capture " + std::string(_color == BLACK ? "black" : "white") + " entity from " + _from.to_string();
  } else {
    return "pass " + std::string(_color == BLACK ? "black" : "white");
  }
}


}