/**
 * @file openxum/core/games/komivoki/board_initializer.cpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2024 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "board_initializer.hpp"
#include "engine.hpp"

#include <random>

namespace openxum::core::games::komivoki {

void FixedBoardInitializer::operator()(komivoki::Engine &engine) const {
  int line = 0;

  for (const auto &e: _configuration) {
    int column = 0;

    for (const auto &value: e) {
      if (value.first != -1) {
        engine.put_entity(komivoki::Coordinates(column, line),
                          new komivoki::Entity(value.first == 0 ? komivoki::BLACK : komivoki::WHITE, value.second));
      }
      ++column;
    }
    ++line;
  }
}

void TwoCompleteLineBoardInitializer::operator()(komivoki::Engine &engine) const {
  int middle = engine.size().first % 2 == 0 ? engine.size().first / 2 - 1 : engine.size().first / 2;

  for (int column = 0; column < engine.size().first; ++column) {
    engine.put_entity(komivoki::Coordinates(column, 1),
                      new komivoki::Entity(komivoki::BLACK, 1));
    engine.put_entity(komivoki::Coordinates(column, engine.size().second - 2),
                      new komivoki::Entity(komivoki::WHITE, 1));
  }
  for (int column = 0; column < middle; ++column) {
    engine.put_entity(komivoki::Coordinates(column, 0),
                      new komivoki::Entity(komivoki::BLACK, 2 + column));
    engine.put_entity(komivoki::Coordinates(engine.size().first - column - 1, 0),
                      new komivoki::Entity(komivoki::BLACK, 2 + column));
    engine.put_entity(komivoki::Coordinates(column, engine.size().second - 1),
                      new komivoki::Entity(komivoki::WHITE, 2 + column));
    engine.put_entity(komivoki::Coordinates(engine.size().first - column - 1, engine.size().second - 1),
                      new komivoki::Entity(komivoki::WHITE, 2 + column));
  }
  if (engine.size().first % 2 == 0) {
    engine.put_entity(komivoki::Coordinates(engine.size().first / 2 - 1, 0),
                      new komivoki::Entity(komivoki::BLACK, 2 + engine.size().first / 2 - 1));
    engine.put_entity(komivoki::Coordinates(engine.size().first / 2, 0),
                      new komivoki::Entity(komivoki::BLACK, 2 + engine.size().first / 2));
    engine.put_entity(komivoki::Coordinates(engine.size().first / 2, engine.size().second - 1),
                      new komivoki::Entity(komivoki::WHITE, 2 + engine.size().first / 2 - 1));
    engine.put_entity(komivoki::Coordinates(engine.size().first / 2 - 1, engine.size().second - 1),
                      new komivoki::Entity(komivoki::WHITE, 2 + engine.size().first / 2));
  } else {
    engine.put_entity(komivoki::Coordinates(engine.size().first / 2, 0),
                      new komivoki::Entity(komivoki::BLACK, 2 + engine.size().first / 2));
    engine.put_entity(komivoki::Coordinates(engine.size().first / 2, engine.size().second - 1),
                      new komivoki::Entity(komivoki::WHITE, 2 + engine.size().first / 2));
  }
}

void CornerBoardInitializer::operator()(komivoki::Engine &engine) const {

  assert(engine.size().first * 2 + 2 > _level and engine.size().second * 2 + 2 > _level);

  for (int k = _level; k > 0; --k) {
    int x = 0;
    int y = _level - k;

    do {
      engine.put_entity(komivoki::Coordinates(x, y), new komivoki::Entity(komivoki::BLACK, k));
      ++x;
      --y;
    } while (y >= 0);
  }
  for (int k = _level; k > 0; --k) {
    int x = engine.size().first - 1;
    int y = engine.size().second - (_level - k) - 1;

    do {
      engine.put_entity(komivoki::Coordinates(x, y), new komivoki::Entity(komivoki::WHITE, k));
      --x;
      ++y;
    } while (y <= engine.size().second - 1);
  }

}

void RandomBoardInitializer::operator()(komivoki::Engine &engine) const {
  std::mt19937 rng(_seed);
  std::uniform_int_distribution<> column_distribution(0, engine.size().first - 1);
  std::uniform_int_distribution<> line_distribution(0, engine.size().second - 1);
  std::uniform_int_distribution<> level_distribution(_level.first, _level.second);

  assert(engine.size().first * engine.size().second >= 2 * _entity_number);

  if ((engine.type() & 0x000F) == VARIANT_NO_CONNECT) {
    Color color = komivoki::BLACK;

    for (unsigned int i = 0; i < 2 * _entity_number; ++i) {
      int column, line;

      do {
        column = column_distribution(rng);
        line = line_distribution(rng);
      } while (not engine.board()[column + line * engine.size().first].empty());
      engine.put_entity(komivoki::Coordinates(column, line),
                        new komivoki::Entity(color, level_distribution(rng)));
      color = color == komivoki::BLACK ? komivoki::WHITE : komivoki::BLACK;
    }
  } else if ((engine.type() & 0x000F) == VARIANT_CONNECT) {
    // TODO
  }
}

}