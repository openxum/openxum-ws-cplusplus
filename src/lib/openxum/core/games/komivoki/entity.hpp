/**
 * @file openxum/core/games/komivoki/entity.hpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2024 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPENXUM_CORE_GAMES_KOMIVOKI_ENTITY_HPP
#define OPENXUM_CORE_GAMES_KOMIVOKI_ENTITY_HPP

namespace openxum::core::games::komivoki {

struct Entity {
  Entity(const Color &color, unsigned int level) : color(color), level(level) {}

  void switch_color() {
    color = color == Color::WHITE ? Color::BLACK : Color::WHITE;
  }

  Color color;
  unsigned int level;
};

}

#endif